<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'savageservices2020');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'tsgMysql');

/** MySQL hostname */
define('DB_HOST', '10.0.0.29');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/* FOR DEVELOPMENT ONLY*/
define('FS_METHOD','direct'); //allows plugin installation on staging
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST']); //allows localhost development to reference staging
define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']); //allows localhost development to reference staging
/* END FOR DEVELOPMENT ONLY*/

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-zp0CD(%a|mwvaUhHXsr0I*(_&x>c:zXr!26o@@YP5$Cp2eZX-Yn=L%% &o.uPQt');
define('SECURE_AUTH_KEY',  '=@&rbcyzts|`S|ZBSksilN.C[3C/(Lc+- -jfGf8;|PA|YRZA@s.XJ5$Y^qRqgC+');
define('LOGGED_IN_KEY',    'E!-+yR(P22tOo889/B]1S`k2b:H770snMp$Ho9Y!k#w-M0C}@%-8Q-x50_d*bcC?');
define('NONCE_KEY',        'v.;_|_w-h!FEKI-e98IFBWv<AJdv63SL:kt~hL[6|yHt4}]*Twuu/+]T87V+:||w');
define('AUTH_SALT',        '|F;q34Wu/7any6awf2pRZE@v7M,^trCDf|=&<`c4-QV~uE#h)y|,pi+du,Y=!v^X');
define('SECURE_AUTH_SALT', '75;^QN*m?[Bgw_r3IK>9 mh% <4M9;rZ$EWm[-}s_)tpIFt&-PU++6}l9,S+DX9;');
define('LOGGED_IN_SALT',   '}2/xi+`n<-UY92U+oFr4?H>WpL3t9]~hu<,2r}f0A0NY{]ep+)JtHP($?6OxfJ2G');
define('NONCE_SALT',       '+-^@sA^{=-<l#-eGLT9-(;-H&53|,Kh_Rx~J|cmzzB1=ijeksoS-wpTu`vvjvk]U');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tsg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');