<?php $contact_ribbon = get_field('contact_us_banner', 'header');
if (trim($contact_ribbon['button_text']) != '') {
  $contact_ribbon['button_text'] = 'Contact Us';
}
$contact_ribbon['href'] = $contact_ribbon['button_location'] == 'Internal' ? get_the_permalink($contact_ribbon['internal_page']) : $contact_ribbon['external_page'];

?>
<div class="contact_ribbon uk-visible@m">
  <a class="button-hover-effect" href="<?php echo $contact_ribbon['href']; ?>"><?php echo $contact_ribbon['button_text']; ?></a>
</div>
<header class="header" role="banner">
  <?php // Line 3 is an example of how to make the navbar sticky with some extra features. To Use add </div> after the </nav> 
  ?>
  <!-- <div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky shadowy; cls-inactive: uk-animation-reverse <?php if (is_front_page()) echo 'uk-navbar-transparent uk-position-top hidemenu'; ?>; top: #body_menu"> -->
  <div class="uk-container-expand uk-margin-auto-left uk-margin-auto-right" style="max-width: 1920px;">
    <div uk-grid class="uk-grid uk-grid-small">
      <div class="uk-width-expand">
        <nav class="" uk-navbar>
          <div class="uk-navbar-left  uk-hidden@m">
            <!-- <a id="mobile_hamburger" class="uk-navbar-toggle uk-hidden@m" uk-toggle="target: #mobile_nav">
              <span uk-icon="icon: menu; ratio: 1.5;" class="uk-animation-fade"></span>
              <span uk-icon="icon: close; ratio: 1.5;" class="uk-animation-fade"></span>
            </a> -->
            <div id="mobile_hamburger" class="uk-navbar-toggle uk-hidden@m"  uk-toggle="target: #mobile_nav">
              <a href="#"><span uk-icon="icon: menu; ratio: 1.5;" class="uk-animation-fade"></span></a>
              <a href="#"><span uk-icon="icon: close; ratio: 1.5;" class="uk-animation-fade"></span></a>
            </div>
            <!-- <a id="mobile_hamburger" class="uk-navbar-toggle uk-hidden@m" uk-toggle="target: #mobile_nav">
              <span uk-icon="icon: menu; ratio: 1.5;" class="uk-animation-fade"></span>
              <span uk-icon="icon: close; ratio: 1.5;" class="uk-animation-fade"></span>
            </a> -->
          </div>
          <div class="uk-navbar-left uk-visible@m">
            <a class="uk-navbar-item uk-logo" href="<?php echo home_url(); ?>">
              <img style="max-height: 100px; width: 143px" src="<?php echo  CHILDPATH; ?>/images/savage-logo.svg" alt="Logo" class="header__logo">
            </a>
          </div>
          <div class="uk-navbar-center uk-hidden@m">
            <a class="uk-navbar-item uk-logo" href="<?php echo home_url(); ?>">
              <img style="max-height: 100px; width: 97px" src="<?php echo  CHILDPATH; ?>/images/savage-logo.svg" alt="Logo" class="header__logo">
            </a>
          </div>
          <div class="uk-navbar-right">
            <?php /* shows in admin as "Primary" */ if (has_nav_menu('menu-1')) wp_nav_menu(array('theme_location' => 'menu-1', 'menu_id' => 'main_menu', 'menu_class' => 'uk-visible@m uk-navbar-nav uk-nav')); ?>
            <div class="contact uk-hidden@m">
              <!-- <span class="arrow-right"></span> -->
              <a class="button-hover-effect" href="<?php echo $contact_ribbon['href']; ?>"><?php echo $contact_ribbon['button_text']; ?></a>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div><?php if (has_nav_menu('menu-2')) : ?></div>
  </div>

  <nav class="uk-container" uk-navbar>
    <div class="uk-nav-bar uk-navbar-center">
      <?php /* shows in admin as "Primary" */ if (has_nav_menu('menu-1')) wp_nav_menu(array('theme_location' => 'menu-1', 'menu_class' => 'uk-nav-center uk-nav')); ?>
    </div>
  </nav>
<?php endif; ?>

<div id="mobile_nav" class="mobileNav" uk-offcanvas="overlay: true; mode: slide;" class="uk-offcanvas uk-hidden@m">
  <div class="uk-offcanvas-bar uk-flex uk-flex-column">
    <?php /* shows in admin as "Primary" */ if (has_nav_menu('menu-3')) wp_nav_menu(array('theme_location' => 'menu-3', 'menu_class' => "uk-nav-left uk-nav", 'menu_id' => 'mainMenuMobile')); ?>
  </div>
</div>
<!-- <div class="uk-container uk-padding-small breadcrumbs_cont"><?php custom_breadcrumbs(); ?></div> -->
</header>