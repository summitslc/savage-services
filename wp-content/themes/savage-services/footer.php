<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Base_Camp
 */

?>

<footer class="footer uk-section-xsmall uk-text-center" role="contentinfo">
    <?php if (get_field('footer', 'footer')) : ?>
    <div class="uk-container uk-padding-small">
        <?php the_field('footer', 'footer', false, false); ?>
    </div>
    <?php endif ?>
    <div class="uk-flex">
        <div class=" footer__copyright">
            <?php echo get_bloginfo('name') ?>&copy; <?php echo date('Y'); ?> <?php esc_html_e('Copyright', 'base-camp'); ?>.
        </div>
        <div class="footer__social">
            <?php if (get_field('linkedin_url', 'socialicons')) : ?>
            <a href="<?php the_field('linkedin_url', 'socialicons'); ?>"><img src="<?php echo CHILDPATH; ?>/images/icons/svg/linkedin.svg" uk-svg></a>
            <?php endif ?>
            <?php if (get_field('facebook_url', 'socialicons')) : ?>
            <a href="<?php the_field('facebook_url', 'socialicons'); ?>"><img src="<?php echo CHILDPATH; ?>/images/icons/svg/fb.svg" uk-svg></a>
            <?php endif ?>
            <?php if (get_field('twitter_url', 'socialicons')) : ?>
            <a href="<?php the_field('twitter_url', 'socialicons'); ?>"><img src="<?php echo CHILDPATH; ?>/images/icons/svg/twitter.svg" uk-svg></a>
            <?php endif ?>
            <?php if (get_field('youtube_url', 'socialicons')) : ?>
            <a href="<?php the_field('youtube_url', 'socialicons'); ?>"><img src="<?php echo CHILDPATH; ?>/images/icons/svg/yt.svg" uk-svg></a>
            <?php endif ?>
            <?php if (get_field('instagram_url', 'socialicons')) : ?>
            <a href="<?php the_field('instagram_url', 'socialicons'); ?>"><?php the_field('instagram_icon', 'socialicons', false, false); ?></a>
            <?php endif ?>
            <?php if (get_field('pinterest_url', 'socialicons')) : ?>
            <a href="<?php the_field('pinterest_url', 'socialicons'); ?>"><?php the_field('pinterest_icon', 'socialicons', false, false); ?></a>
            <?php endif ?>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>