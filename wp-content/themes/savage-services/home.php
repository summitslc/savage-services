<?php

/**
 * Blog posts index page
 */
/*Swap this with the code in home-no-slider.php  if you don't want a slider */

get_header(); ?>

<main role="main" class="main news_feed blue_tri tech_background">
    <section class="uk-section news">
        <div class="uk-container">
            <h1><?php single_post_title(); ?></h1>
            <div class="uk-grid-medium" uk-grid>
            
                <aside class="uk-width-1-4@l uk-width-1-5@m uk-visible@m">
                    <p class="title">Category</p>
                            <ul class="uk-list uk-text-small">
                                <?php wp_list_categories([
                                    'title_li' => '',
                                    'show_option_all' => 'All'
                                ]); ?>
                            </ul>
                </aside>
                <div class="uk-width-3-4@l uk-width-4-5@m">
                    <?php if (have_posts()) : ?>
                    <div class="uk-child-width-1-3@s uk-child-width-1-2 uk-grid-small" uk-grid>
                    
                        <?php while (have_posts()) : the_post(); ?>
                            <!-- article -->
                            <div class="article_wrapper">
                            <?php get_template_part('includes/post' , 'single'); ?>
                            </div>
                            <!-- /article -->
                        <?php endwhile; ?>
                    </div>

                        <?php get_template_part('includes/pagination'); ?>
                    <?php else : ?>
                        <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
function produce_XML_object_tree($raw_XML) {
    libxml_use_internal_errors(true);
    try {
        $xmlTree = new SimpleXMLElement($raw_XML);
    } catch (Exception $e) {
        // Something went wrong.
        $error_message = 'SimpleXMLElement threw an exception.';
        foreach(libxml_get_errors() as $error_line) {
            $error_message .= "\t" . $error_line->message;
        }
        trigger_error($error_message);
        return false;
    }
    return $xmlTree;
}

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}


$xml_feed_url = "https://app.meltwater.com/gyda/outputs/5e0f306fdf6d4ecd0d228945/rendering?apiKey=57cf5025eb2d0f00037eb637&type=rss";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $xml_feed_url);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$xml = curl_exec($ch);
curl_close($ch);

$xml = produce_XML_object_tree($xml);
?>
    
    <section class="uk-section uk-section-xsmall shadow-all media_feed uk-section-default">
        <div class="uk-container">
            <div class="uk-grid-medium" uk-grid>
                <div class="uk-width-1-4@l uk-width-1-5@m uk-visible@m">&nbsp;</div>
                <div class="uk-width-3-4@l uk-width-4-5@m">

                    <h3>Recent Media Coverage</h3>


                    <div uk-slider="sets: true">

                        <div class="uk-position-relative" tabindex="-1">
                            <div class="uk-slider-container">

                                
                                <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-medium"  uk-height-match="target: .rmc__title" uk-grid >

                                    <?php
                                    $totalItems = 0;
                                    foreach($xml->channel->item as $item){
										$totalItems++;
										if($totalItems > 9) break;

										$title = (string) $item->title;
                                        $desc = (string) $item->description;
                                        $url = $item->guid;
                                        $date = date( 'M j, Y', strtotime( $item->pubDate ) );
                                        
                                        ?>
                                        <li>
                                            <article class="rmc uk-article">
                                                <p class="rmc__meta uk-article-meta"><?php echo $date; ?></p>
                                                <h2 class="rmc__title uk-article-title"><a class="uk-link-reset" href="<?php echo $url; ?>"><?php echo $title; ?></a></h2>
                                                <p class="rmc__desc"><?php echo limit_text($desc, 20); ?></p>
                                                <a class="rmc__action uk-button uk-button-small button-hover-effect" href="<?php echo $url; ?>">more</a>
                                            </article>
                                        </li>

                                    <?php $z++; } ?>
                                
                                </ul>
                            </div>

                            <a class="uk-position-center-left-out" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                            <a class="uk-position-center-right-out" href="#" uk-slidenav-next uk-slider-item="next"></a>

                        </div>

                    </div>
                </div>
            </div>      
        </div>
    </section>

    <section class="uk-section uk-section-small"></section>

</main>

<?php get_footer(); ?>