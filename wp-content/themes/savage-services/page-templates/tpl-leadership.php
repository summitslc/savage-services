<?php /* Template Name: Leadership */
get_header(); ?>

<main role="main" class="tech_background">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php get_template_part('includes/headers', 'large'); ?>
            <section class="uk-section">
                <div class="uk-container">


                    <?php if (have_rows('leadership_section')) : ?>
                        <?php $leaderID = 0; ?>
                        <section id="leaders">
                            <?php while (have_rows('leadership_section')) : the_row(); ?>
                                <h2><?php the_sub_field('leadership_section_title'); ?></h2>
                                <?php if (have_rows('leaders')) : ?>
                                    <div class="child-width-1-7d-1-2m" uk-grid>
                                        <?php while (have_rows('leaders')) : the_row(); ?>
                                            <div class="leader"> <a href="#leader<?php echo $leaderID; ?>" class="uk-link-heading" uk-toggle>
                                                    <?php if (get_sub_field('leader_image')) { ?>
                                                        <img src="<?php the_sub_field('leader_image'); ?>" />
                                                    <?php } ?>
                                                    <h3>
                                                        <?php the_sub_field('leader_name'); ?>
                                                    </h3>
                                                    <p>
                                                        <?php the_sub_field('leader_title'); ?></p>
                                                </a>
                                            </div>
                                            <!-- Modal content-->
                                            <div id="leader<?php echo $leaderID;
                                                            $leaderID++ ?>" uk-modal>
                                                <div class="uk-modal-dialog">
                                                    <button class="uk-modal-close uk-position-top-right" type="button"><span>Close&nbsp;&nbsp;</span> <img src="<?php echo  CHILDPATH; ?>/images/icons/svg/circle-close.svg" uk-svg /></button>
                                                    <div class="uk-container" uk-grid uk-overflow-auto>
                                                        <div class="leader uk-width-auto@s">
                                                            <img src="<?php the_sub_field('leader_image'); ?>" />
                                                                <h3>
                                                                    <?php the_sub_field('leader_name'); ?>
                                                                </h3>
                                                                <p>
                                                                    <?php the_sub_field('leader_title'); ?></p>
                                                        </div>
                                                        <div class="uk-width-expand@s">
                                                            <div class="leader-bio uk-padding-remove-right uk-padding-remove-top">
                                                                <p><?php
                                                                    the_sub_field('leader_bio'); ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php else : ?>
                                    <?php // no rows found 
                                    ?>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        </section>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>

                </div>
            </section>
        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>