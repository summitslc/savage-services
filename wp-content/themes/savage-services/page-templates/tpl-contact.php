<?php /* Template Name: Contact */
get_header();
/** This currently works, but the way some classes are named and possibly the way the divs are laid out could use some refactoring */
?>

<main role="main" class="blue_tri rail-bg uk-padding-remove-vertical">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <section class="uk-container uk-container-large uk-padding-remove uk-section">
                <!-- uk-height-match should be removed if the content is usually significantly shorter than 100vh -->
                <div class="uk-height-1-1 uk-grid-collapse container-has-edge-triangles" uk-grid uk-height-match>
                    <div class="triangles-outer triangles-outer--left uk-visible@m">
                        &nbsp;
                    </div>
                    <div class="overlay-edge-triangles">
                        <div class="overlay-edge-triangles-content uk-padding">
                            <h2><?php echo get_the_title() ?></h2>
                            <?php the_content(); ?>
                            <iframe src="http://go.pardot.com/l/849103/2020-04-16/3w9q" width="100%" height="650" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
                            <div class="contact-half-blocks" uk-grid>
                                <div class="uk-width-1-2@s">
                                    <h3><?php the_field('left_content_title'); ?></h3>
                                    <div><?php the_field('left_content'); ?></div>
                                    <a href="<?php the_field('left_content_cta_url'); ?>" class="uk-text-small button-hover-effect uk-text-uppercase"><?php the_field('left_content_cta_text'); ?></a>
                                </div>
                                <div class="uk-width-1-2@s">
                                    <h3><?php the_field('right_content_title'); ?></h3>
                                    <div><?php the_field('right_content'); ?></div>
                                    <a href="<?php the_field('right_content_cta_url'); ?>" class="uk-text-small button-hover-effect uk-text-uppercase"><?php the_field('right_content_cta_text'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="triangles-outer triangles-outer--right uk-visible@m">
                        &nbsp;
                    </div>
                </div>

            </section>
        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>