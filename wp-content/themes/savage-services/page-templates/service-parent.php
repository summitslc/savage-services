<?php /* Template Name: Service (top level) */ 
get_header(); ?>

<main role="main" class="service_parent tech_background">

    <?php if (have_posts()){ while (have_posts()){ the_post(); ?>
    
        <?php get_template_part('includes/headers', 'large'); ?>

        <?php if ( have_rows( 'service' ) ) : ?>

            <section class="uk-section">
                <div class="uk-container">

                <div class="uk-flex-between uk-grid-small uk-child-width-1-4@m uk-child-width-1-2@s uk-child-width-1-1" uk-grid  uk-height-match="target: .top">
                    <?php while ( have_rows( 'service' ) ) : the_row(); ?>
                        <div>
                            <div class="top">
                                <img src="<?php echo CHILDPATH . '/images/icons/svg/'.get_sub_field( 'icon' ).'.svg'?>" uk-svg>

                                <h3><?php the_sub_field( 'title' ); ?></h3>
                            </div>
                            <?php $pages = get_sub_field( 'pages' ); ?>
                            <?php if($pages){ ?>
                            <ul class="uk-list">
                                <?php foreach ($pages as $page_id) { ?>
                                    <li>
                                        <a class="btn" href="<?php the_permalink( $page_id ); ?>"><?php echo get_the_title( $page_id ); ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <?php } ?>

                        </div>
                    <?php endwhile; ?>
                </div>


                </div>
            </section>
        <?php endif; ?>
        
    <?php } } ?>

</main>

<?php get_footer(); ?>