<?php /* Template Name: Industries */
get_header(); ?>

<main role="main" class="tech_background">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php get_template_part('includes/headers', 'large'); ?>
            <section class="uk-section uk-padding-remove-vertical">
                <div class="uk-container">
                    <?php if (have_rows('industries')) : ?>
                        <?php $industryID = 0; ?>
                        <section class="industry-container uk-section uk-container">
                            <div class="uk-grid-small uk-grid-match uk-flex-nowrap uk-grid" uk-grid><!-- industries-jumplinks -->
                                <aside class="sibling_sidebar uk-width-1-5 uk-visible@m">
                                    <div class="width-element" uk-sticky>
                                        <p class="title">Jump To:</p>
                                        <ul class="uk-list uk-text-small">
                                            <!-- automaticaly creates the jump links -->
                                            <?php
                                            $linkIndex = 0;
                                            while (have_rows('industries')) : the_row(); ?>
                                                <li>
                                                    <a href="#industry<?php echo $linkIndex;
                                                                        $linkIndex++ ?>" uk-scroll><?php the_sub_field('industry_name'); ?></a>
                                                </li>
                                            <?php endwhile ?>
                                        </ul>
                                    </div>
                                </aside>
                                <div class="main_content uk-width-expand">
                                    <ul uk-accordion="multiple: true">
                                        <?php while (have_rows('industries')) : the_row(); ?>
                                            <li class="<?php if($industryID == 0){echo 'uk-open';} ?>">
                                                <a class="uk-accordion-title uk-hidden@m" href="#"><?php the_sub_field('industry_name'); ?></a>
                                                <div id="industry<?php echo $industryID;
                                                                    $industryID++ ?>" class="industry uk-accordion-content" uk-grid>
                                                    <div class="uk-width-auto@m ">
                                                        <?php $industry_image = get_sub_field('industry_image'); ?>
                                                        <?php if ($industry_image) { ?>
                                                            <img src="<?php echo $industry_image['url']; ?>" alt="<?php echo $industry_image['alt']; ?>" />
                                                        <?php } ?>
                                                    </div>
                                                    <div class="uk-width-expand uk-padding-remove-top uk-padding-small">
                                                        <h2 class="uk-visible@m"><?php the_sub_field('industry_name'); ?></h2>
                                                        <?php the_sub_field('industry_description'); ?>
                                                        <?php if (have_rows('industry_ctas')) : ?>
                                                            <?php while (have_rows('industry_ctas')) : the_row(); ?>
                                                                <a href="<?php the_sub_field('industry_cta_url'); ?>" class="button-hover-effect industry_cta"><?php the_sub_field('industry_cta_text'); ?></a>
                                                            <?php endwhile; ?>
                                                        <?php else : ?>
                                                            <?php // no rows found 
                                                            ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>

                                            </li>
                                        <?php endwhile; ?>
                                </div>
                            </div>
                        </section>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>

                </div>
            </section>
        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>