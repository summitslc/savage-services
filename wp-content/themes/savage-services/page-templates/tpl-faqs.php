<?php /* Template Name: FAQs */
get_header(); ?>

<main role="main" class="tech_background blue_tri">
    <section class="uk-section">
        <div class="uk-container faq-title">
            <h1><?php echo get_the_title() ?></h1>
            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    the_content();
                }
            }
            ?>
        </div>
            <?php if (have_rows('faq_sections')) : ?>
                <?php while (have_rows('faq_sections')) : the_row(); ?>
                    <section class="faq-container uk-section not-muted-s uk-border-rounded uk-container  uk-padding-remove-horizontal uk-padding-remove-vertical" >
                        <div>
                            <div class="uk-padding uk-padding-remove-bottom">
                                <h2><?php the_sub_field('faq_section_name'); ?></h2>
                                <hr class="uk-visible@m">
                            </div>
                            <hr class="mobile-divider uk-hidden@m">
                            <div class="faq-grid uk-padding uk-padding-remove-top" uk-grid>
                                <div class="uk-width-auto uk-visible@m"><?php $faq_section_image = get_sub_field('faq_section_image'); ?>
                                    <?php if ($faq_section_image) { ?>
                                        <img src="<?php echo $faq_section_image['url']; ?>" alt="<?php echo $faq_section_image['alt']; ?>" /></div>
                            <?php } ?>
                            <?php if (have_rows('faqs')) : ?>
                                <div class="uk-width-expand uk-padding-remove-top">
                                    <ul class="faq-list" uk-accordion>
                                        <?php while (have_rows('faqs')) : the_row(); ?>
                                            <li>
                                                <a class="uk-accordion-title" href="#"><?php the_sub_field('faq_question'); ?></a>
                                                <div class="uk-accordion-content"><?php the_sub_field('faq_answer'); ?></div>
                                                <!-- <hr> -->
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php else : ?>
                    <?php // no rows found 
                    ?>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php else : ?>
            <?php // no rows found 
            ?>
        <?php endif; ?>
    </section>
</main>

<?php get_footer(); ?>