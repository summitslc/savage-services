<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Base_Camp
 */

get_header(); ?>

<main role="main" class="default_page blue_tri tech_background">
    <?php if (have_posts()){ while (have_posts()){ the_post(); ?>


    <section class="uk-section">
        <div class="uk-container">

            <div class="uk-grid-small uk-grid-match uk-flex-nowrap" uk-grid>
                <!-- Sticks until the bottom of its parent container -->
                <?php if($post->post_parent == 114){ // 114 = services ?>
                    <aside class="sibling_sidebar uk-width-1-5 uk-visible@m">
                        <div class="uk-height-1-1">
                            <div uk-sticky="offset: 48; bottom: true;"> 
                                <p class="title">Services</p>
                                <ul class="uk-list uk-text-small">
                                    
                                    <?php 
                                    wp_list_pages(array(
                                        'child_of' => $post->post_parent,
                                        // 'exclude' => $post->ID,
                                        'depth' => 1,
                                        'title_li' => '',
                                        'sort_column' => 'menu_order'
                                    ));
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </aside>
                <?php } ?>


                <article class="main_content uk-width-expand">
                    <div class="uk-height-1-1">
                        <div class="uk-grid-small" uk-grid>
                            <header class="uk-width-1-1">
                                <h1 uk-title>
                                    <?php the_title(); ?>
                                </h1>
                            </header>
                            <div class="uk-width-1-1">
                                <?php get_template_part( 'includes/headers'  , 'small' );; ?>
                            </div>
    
                            <div class="page__content uk-width-expand">
                                <?php
                                the_content();
                                ?>
                            </div>
                            <?php get_template_part('includes/services', 'sidebar'); ?>
                        </div>
                    </div>
                </article>
            </div>

        </div>
    </section>
    <?php } } ?>
</main>


<?php get_footer(); ?>