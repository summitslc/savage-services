<?php // WordPress recoginzes this as the index page of a site.
get_header();?>
<main role="main" class="home">

<?php 
if ( have_posts() ) : 
    while ( have_posts() ) : the_post(); 
        get_template_part( 'includes/home/slider' );
        ?>
        <div class="shadow-top-show@s">
            <?php get_template_part( 'includes/home/our-services' ); ?>
        </div>
        <div class="shadow-top  shadow-bottom-hide@s">
            <?php get_template_part( 'includes/home/who-we-are' ); ?>
        </div>
        <div class="shadow-top-show@s">
            <?php get_template_part( 'includes/home/recent-news' ); ?>
        </div>
        <?php
    endwhile; 
endif; 
?>
    

    


</main>

<?php get_footer();?>