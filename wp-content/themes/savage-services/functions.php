<?php
/** GOOGLE ANALYTICS TAGS, these will be the UA-XXXXXXXX-XX codes */
define('GA_CLIENT', '');
define('GA_TSG', '');

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles', 11);
function my_theme_enqueue_styles() {
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('child-style',
        get_stylesheet_directory_uri() . '/style.min.css',
        array('base-camp-style-custom'),
        wp_get_theme()->get('Version')
    );
    if( is_page( 'industries' ) ){
        wp_enqueue_script( 'open-accordion-desktop',  CHILDPATH . '/js/open-accordion-desktop.js', array(), '1.0.0', true );
    }

    wp_enqueue_script('savage', CHILDPATH . '/js/scripts.js', array(), '0.0.1', true);
    
    // wp_enqueue_style('slick-slider', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
    // wp_enqueue_script('slick-slider', '/cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), '0.0.1', true);
}

function add_image_sizes() {
    //add_image_size('size_name', width, height, crop); // Small Thumbnail
}
add_action('after_setup_theme', 'add_image_sizes');

function add_new_nav_menus() {

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
    'menu-3' => esc_html__( 'Mobile', 'base-camp' ),
    ) );

}
add_action('after_setup_theme', 'add_new_nav_menus');


/**
 * This function is to remove the extra p tags and line breaks for my shortcodes
 */
if( !function_exists('wpex_fix_shortcodes') ) {
	function wpex_fix_shortcodes($content){   
		$array = array (
			'<p>[' => '[', 
			']</p>' => ']', 
			']<br />' => ']'
		);
		$content = strtr($content, $array);
		return $content;
	}
	add_filter('the_content', 'wpex_fix_shortcodes');
}



function button_shortcode($atts) {
    $a = shortcode_atts(array(
        'link' => '#',
        'target' => null,
        'text' => 'Learn More',
    ), $atts);

    return '<a href="' . $a['link'] . '" ' . ($a['target'] !== null ? ' target="_blank" ' : '') . ' class="btn">' . $a['text'] . '</a>';
}

add_shortcode('button', 'button_shortcode');

function two_column_shortcode($atts, $content) {
    $a = shortcode_atts(array(), $atts);
    // $content = wpex_fix_shortcodes(trim($content));

    return '<div class="double_column uk-grid-small uk-child-width-1-2@s" style="padding-top: 15px;" uk-grid>'.do_shortcode($content).'</div>';
    // return '<a href="' . $a['link'] . '" ' . ($a['target'] !== null ? ' target="_blank" ' : '') . ' class="btn">' . $a['text'] . '</a>';
}
add_shortcode('two_column', 'two_column_shortcode');

function single_column_shortcode($atts, $content) {
    $a = shortcode_atts(array(), $atts);
    // $content = wpex_fix_shortcodes(trim($content));

    return '<div class="single_column">'.$content.'</div>';
    // return '<a href="' . $a['link'] . '" ' . ($a['target'] !== null ? ' target="_blank" ' : '') . ' class="btn">' . $a['text'] . '</a>';
}
add_shortcode('column', 'single_column_shortcode');

function intro_text($atts, $content) {
    $a = shortcode_atts(array(), $atts);
    

    return '<div class="intro uk-text-lead">'.$content.'</div>';
    // return '<a href="' . $a['link'] . '" ' . ($a['target'] !== null ? ' target="_blank" ' : '') . ' class="btn">' . $a['text'] . '</a>';
}
add_shortcode('intro', 'intro_text');

// Filter Functions with Hooks
function custom_mce_button() {
    // Check if user have permission
    if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
        return;
    }
    // Check if WYSIWYG is enabled
    if ('true' == get_user_option('rich_editing')) {
        add_filter('mce_external_plugins', 'custom_tinymce_plugin');
        add_filter('mce_buttons', 'register_mce_button');
    }
}
add_action('admin_head', 'custom_mce_button');

// Function for new button
function custom_tinymce_plugin($plugin_array) {
    $plugin_array['custom_mce_button'] = CHILDPATH . '/editor_plugin.js';
    return $plugin_array;
}

// Register new button in the editor
function register_mce_button($buttons) {
    array_push($buttons, 'custom_mce_button');
    return $buttons;
}

include('includes/admin/admin-functions.php');

/*
 *
 * Custom ACF Location Type
 * acf_location_rules_types -> Adds new "Hidden" type
 * acf_location_rule_values_hidden -> Makes the only Choice "Hidden"
 * acf_location_rule_match_hidden -> Disables the group from showing up anywhere on the site, that way it can be cloned
 *
 * This is used for groups that we would re-use, specifically a button group (with choices for internal/external and logic to match);
 * Also being used for re-usable sections on pages
 *
 */
add_filter('acf/location/rule_types', 'acf_location_rules_types');
function acf_location_rules_types($choices) {
    $choices['Custom']['hidden'] = 'Hidden';
    return $choices;
}

add_filter('acf/location/rule_values/hidden', 'acf_location_rule_values_hidden');
function acf_location_rule_values_hidden($choices) {
    $choices['clonable'] = 'Hidden';
    return $choices;
}

add_filter('acf/location/rule_match/hidden', 'acf_location_rule_match_hidden', 10, 4);
function acf_location_rule_match_hidden($match, $rule, $options, $field_group) {
    return false;
}

// Used to add +/- to mobile nav
add_filter( 'walker_nav_menu_start_el', 'cfw_add_arrow',10,4);
function cfw_add_arrow( $output, $item, $depth, $args ){
    //Only add class to 'top level' items on the 'primary' menu.
    if( ('mainMenuMobile' == $args->menu_id || 'secondaryMenuMobile' == $args->menu_id) ){
        // var_dump($args);
        // echo "<br>";
        if (in_array("menu-item-has-children", $item->classes)) {
            $output = substr_replace ($output, " <span class='plus'>+</span></a>", -4, 0);
        }
    }
    return $output;
}
