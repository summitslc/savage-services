<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Base_Camp
 */

get_header(); ?>


<?php /* Template Name: Service (lower) */ 
get_header(); ?>

<main role="main" class="single_post blue_tri tech_background">
    <?php if (have_posts()){ while (have_posts()){ the_post(); ?>


    <section class="uk-section">
        <div class="uk-container">

            <div class="uk-grid-small uk-grid-match uk-flex-nowrap" uk-grid>
                <!-- Sticks until the bottom of its parent container -->
                <aside class="sibling_sidebar uk-width-1-5 uk-visible@m">
                    <div class="uk-height-1-1">

                        <p class="title">Category</p>
                        <ul class="uk-list uk-text-small">
                            <?php wp_list_categories([
                                'title_li' => '',
                                'show_option_all' => 'All'
                                ]); ?>
                            </ul>
                        </div>
                </aside>

                <article class="main_content uk-width-expand">
                    <header>
                        <p class="uk-article-meta uk-margin-remove-bottom">
                            <span class="post__date"><?php the_date(); ?></span>
                        </p>
                        <h1 uk-title class="uk-margin-remove-top">
                            <?php the_title(); ?>
                        </h1>
                    </header>
                    <?php if( has_post_thumbnail( ) ){ ?>
                            <div class="uk-cover-container">
                                <img src="<?php echo get_the_post_thumbnail_url($page->ID, 'full'); ?>">
                            </div>
                    <?php } ?>

                        <div class="page__content uk-width-expand">
                            <?php
                            the_content();
                            ?>
                        </div>
                </article>
            </div>

        </div>
    </section>
    <?php } } ?>
</main>

<?php get_footer(); ?>