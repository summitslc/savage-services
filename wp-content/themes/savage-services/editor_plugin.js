(function () {
    tinymce.PluginManager.add('custom_mce_button', function (editor, url) {
        editor.addButton('custom_mce_button', {
            icon: 'link',
            text: 'Button',
            onclick: function () {
                editor.windowManager.open({
                    title: 'Insert Button',
                    body: [
                        {
                            type: 'textbox',
                            name: 'url',
                            label: 'URL',
                            onclick: function (e) {
                                jQuery(e.target).css('box-shadow', '');
                            }
                        },
                        {
                            type: 'textbox',
                            name: 'buttonText',
                            label: 'Button Text'
                        },
                        {
                            type: 'checkbox', // component type
                            name: 'external', // identifier
                            label: 'Open in new tab', // text for the label
                          }
                    ],
                    onsubmit: function (e) {
                        email_regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
                        if (e.data.url.trim() == "" || !email_regexp.test(e.data.url)) {
                            e.preventDefault();
                            var inputs = jQuery('#' + this._id + '-body').find('.mce-formitem input');
                            jQuery(inputs[0]).css('box-shadow', '0px 0 0 2px #ec2147');
                            editor.windowManager.alert('URL is required and must be of proper format', function(){});
                            return false;
                        }
                        link = ' link="' + e.data.url + '" ';
                        text = e.data.buttonText.trim() == '' ? '' : ' text="' + e.data.buttonText + '" ';
                        external = e.data.external ? ' target="external" ' : '';

                        editor.insertContent(
                            '[button' + link + text + external + ']'
                        );
                    }
                });
            }
        });
    });
})();