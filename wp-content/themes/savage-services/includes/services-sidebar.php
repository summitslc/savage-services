<?php if ( have_rows( 'sidebar_content' ) ): ?>

<div class="right_sidebar uk-width-1-1 uk-width-1-3@s uk-width-medium@m">
    <div class="uk-grid-small uk-margin-remove uk-child-width-1-1" uk-grid>

        <?php while ( have_rows( 'sidebar_content' ) ) : the_row(); ?>

            <?php if ( get_row_layout() == 'pdf_download' ) : ?>
            <?php $pdf = get_sub_field( 'pdf' ); ?>
            <?php $thumbnail_image = get_sub_field( 'thumbnail_image' ); ?>

            <div class="pdf_download uk-card uk-card-small uk-card-body">
                <strong><?php the_sub_field( 'title' ); ?></strong>
                <div class="pdf_image">
                    <a class="pdf_link" href="<?php echo $pdf['url']; ?>">
                            <img src="<?php echo $thumbnail_image['url']; ?>" alt="<?php echo $pdf['filename']; ?>" />
                    </a>
                </div>
                <div class="actions">
                    <a class="btn" href="<?php echo $pdf['url']; ?>">Download PDF</a>
                </div>
            </div>


            <?php elseif ( get_row_layout() == 'rich_text' ) : ?>

            <div class="rich_text uk-card uk-card-small uk-card-body <?php the_sub_field( 'background_color' ); ?>">
                <?php the_sub_field( 'content' ); ?>
            </div>
            
            <?php elseif ( get_row_layout() == 'video' ) : ?>
                <?php $placeholder_image = get_sub_field( 'placeholder_image' ); ?>
                <?php $sidebar_video = get_sub_field( 'video' ); ?>

                <?php $video_url = $sidebar_video[$sidebar_video['location'] == "external"?'url':'file']; ?>

                <div class="video uk-card uk-card-small" uk-lightbox>
                    <p><?php the_sub_field( 'intro_text' ); ?></p>
                    <a class="" href="<?php echo $video_url; ?>" data-caption="" data-attrs="width: 1280; height: 720;">
                        <div class="uk-cover-container has_overlay">
                            <canvas width="270" height="150"></canvas>
                            <img data-src="<?php echo wp_get_attachment_image_src( $placeholder_image, 'full' )[0] ; ?>" data-width="467" data-height="263" alt="" uk-img uk-cover>
                        </div>
                    </a>
                </div>

            <?php endif; ?>
        
        <?php endwhile; ?>

    </div>
</div>

<?php endif; ?>