<?php
add_action('admin_head', 'custom_admin_styling');

function custom_admin_styling() {
    global $post;
    //home_slide_preview is for backend use to help users see the results of putting the overlay on their slides
    ?>
    <style>
        .home_slide_preview .image-wrap, .video_image .image-wrap {
            position:  relative;
            overflow: hidden;
            box-shadow: 0 0 10px lightgrey;
        }
        .home_slide_preview.show_overlay .image-wrap::before {
            opacity: 1;
        }
        .home_slide_preview .image-wrap::before {
            content: '';
            display: block;
            width: 48.5%;
            height: 105%;
            left: 0;
            background: #3c4980;
            position:  absolute;
            z-index: 1;
            transform: skew(-22deg);
            transform-origin: top left;
            opacity: 0;
            transition: opacity 0.35s ease;
            pointer-events: none;
        }
        .home_slide_preview.show_overlay .image-wrap::after {
            opacity: 1;
        }
        .home_slide_preview .image-wrap::after {
            content: '';
            display: block;
            width: 100%;
            height: 25%;
            left: 0;
            bottom: -1%;
            background: white;
            position:  absolute;
            z-index: 2;
            transform: skewY(-2.9deg);
            transition: opacity 0.35s ease;
            transform-origin: top right;
            opacity: 0;
            pointer-events: none;
        }
        .video_image.show_overlay .image-wrap::after {
            opacity: 1;
        }
        .video_image .image-wrap::after {
            content: '';
            display: block;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            background: rgba(0,0,0,0.25);
            position:  absolute;
            z-index: 1;
            transition: opacity 0.35s ease;
            opacity: 0;
            pointer-events: none;
        }

            .featured_overlay #postimagediv p.hide-if-no-js:first-of-type:not(:last-of-type) #set-post-thumbnail{
                position: relative;
            }
            .featured_overlay #postimagediv p.hide-if-no-js:first-of-type:not(:last-of-type) #set-post-thumbnail::after{
                    content: '';
                    display: block;
                    position: absolute;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    background-image: url('<?php echo CHILDPATH; ?>/images/icons/png/header-small-overlay.png');
                    background-size: 100% 100%;
                    pointer-events: none;
            }
        
        
    </style>
    <script>
    (function($) {
        acf.addAction('new_field/type=image', function(field){ // find 'home_side_preview' fields and add functionality to show/hide overlay in admin
            if(field.$el.hasClass('home_slide_preview') || field.$el.hasClass('video_image')){
                var overlay = field.$el.siblings('.custom_overlay');

                if(overlay){
                    $(overlay.find('input[type="checkbox"]')).change(function(){
                        if($(this).prop('checked')){
                            field.$el.addClass('show_overlay');
                        }else{
                            field.$el.removeClass('show_overlay');
                        }
                    });
                    
                    if( $( overlay.find('input[type="checkbox"]') ).prop('checked') ){
                        field.$el.addClass('show_overlay');
                    }
                }
            }
        });
        $(document).ready(toggle_body_class);
        $(document).on('change','#page_template',toggle_body_class);
        function toggle_body_class(){
            if( $('#page_template')[0].value == 'default' ){
                $('#poststuff').addClass('featured_overlay');
            } else {
                $('#poststuff').removeClass('featured_overlay');
            }
        }
    })(jQuery);

    </script>
    <?php
}