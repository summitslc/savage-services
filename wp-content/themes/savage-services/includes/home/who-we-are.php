<section class="home_who_we_are uk-section">
    <div class="uk-container">
        <div class="uk-flex uk-flex-between@s uk-child-width-1-2@s" uk-grid>

            <?php $left_text = get_field('wwa_text'); ?>
            
            <div class="left_text">
                <header>
                    <h2 class="uk-margin-remove-bottom"><?php echo $left_text['title'] ?></h2>
                </header>
                <p><?php echo $left_text['copy'] ?></p>
                <?php $button_href = $left_text['button']['button_location'] == 'Internal'? get_the_permalink( $left_text['button']['internal_page'] ) : $left_text['button']['external_page'] ; ?>
                <p><a href="<?php echo $button_href; ?>" class="btn"><?php echo $left_text['button']['button_text'] ?></a></p>
            </div>
            

            <?php $right_video = get_field('wwa_video'); ?>
            <?php $video_url = $right_video['video'][$right_video['video']['location'] == "external"?'url':'file']; ?>
            
            <div class="right_vid" uk-lightbox>
                <a class="" href="<?php echo $video_url; ?>" data-caption="" data-attrs="width: 1280; height: 720;">
                    <div class="uk-cover-container<?php if( $right_video['overlay'] ){echo ' has_overlay';} ?>">
                        <canvas width="467" height="263"></canvas>
                        <img data-src="<?php echo $right_video['image']['url']; ?>" data-width="467" data-height="263" alt="" uk-img uk-cover>
                    </div>
                </a>
                <p><?php echo $right_video['copy']; ?></p>
            </div>
        </div>
    </div>
</section>