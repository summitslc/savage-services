<?php if (have_rows('os_services')): ?>
<section class="home_our_services uk-section">
    <div class="uk-container">
        <header>
            <h2><?php the_field('os_title'); ?></h2>
        </header>

        <div class="switcher_buttons uk-hidden@s uk-grid-small" uk-grid uk-switcher="animation: uk-animation-fade; toggle: > *" >
            <?php while ( have_rows( 'os_services' ) ) : the_row(); ?>
                <div>
                    <a>
                        <img src="<?php echo CHILDPATH . '/images/icons/svg/'.get_sub_field( 'icon' ).'.svg'?>" uk-svg>
                    </a>
                </div>
            <?php endwhile; ?>
        </div>


        <div class="uk-switcher uk-flex-center uk-child-width-1-4@m uk-child-width-1-2@s uk-child-width-1-1 uk-grid-medium" uk-grid>
            <?php while (have_rows('os_services')): the_row();?>
            <div class="match_height">
                <div>
                    <div class="uk-visible@s">
                        <img src="<?php echo CHILDPATH . '/images/icons/svg/'.get_sub_field( 'icon' ).'.svg'?>" uk-svg>
                    </div>
                    <h3 class=""><?php the_sub_field('title');?></h3>
                    <p class=""><?php the_sub_field('subtitle');?></p>
                </div>

                <?php $button = get_sub_field('button'); ?>
                <?php $button_href = $button['button_location'] == 'Internal'? get_the_permalink( $button['internal_page'] ) : $button['external_page'] ; ?>
                <a href="<?php echo $button_href; ?>" class="btn"><?php echo $button['button_text']; ?></a>
            </div>
            <?php endwhile;?>
        </div>
    </div>
</section>


<?php endif;?>