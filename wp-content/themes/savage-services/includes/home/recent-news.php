
<section class="recent_news uk-section">
    <div class="uk-container">
        <header>
            <h2><?php the_field( 'rn_title' ); ?>
                <a class="uk-text-small button-hover-effect uk-text-uppercase" href="<?php echo get_post_type_archive_link('post'); ?>"><?php the_field( 'rn_see_all_button' ); ?></a>
            </h2>
        </header>
        <?php

$featured = get_field( 'rn_featured_stories' ) ?: [];

$args = array( //this gets 3 most recent posts that aren't the featured (only returns ID)
    'category__in' => get_all_category_ids(), //for some reason it's not working unless category is included
    'posts_per_page' => 3,
    'no_found_rows' => true,
    'post_status' => 'publish',
    'post__not_in' => $featured,
    'ignore_sticky_posts' => true,
    'fields' => 'ids',
);

$recent_ids = array_merge($featured, get_posts($args));
array_splice($recent_ids, 3); //only want three of them

// the query
$the_query = new WP_Query(array( // actually get the posts to loop through
    'post__in' => $recent_ids,
    'orderby' => 'post__in',
    'posts_per_page' => -1,
    'ignore_sticky_posts' => true,
));
?>

        <?php if ($the_query->have_posts()): ?>
        <div class="uk-position-relative" tabindex="-1" uk-slider="sets: true">

            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m uk-grid-collapse" uk-grid>
                <?php while ($the_query->have_posts()): $the_query->the_post();?>
                    <li>
                        <?php get_template_part('includes/recent_news', 'single');?>
                    </li>
                <?php endwhile;?>
                <?php wp_reset_postdata();?>
            </ul>

            <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-padding-small-top"></ul>

        </div>

        <?php else: ?>
        <p><?php __('No News');?></p>
        <?php endif;?>

    </div>
</section>