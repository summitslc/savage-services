<?php if ( have_rows( 'hps_slides' ) ) : ?>
<section class="home_page_slider uk-section uk-padding-remove">
    <div class="uk-position-relative uk-visible@m" tabindex="-1" uk-slideshow="ratio: 24:5;animation: fade; min-height: 225; autoplay: true">
        
        <div class="overlay">
            <ul class="uk-slideshow-items">

                <?php while ( have_rows( 'hps_slides' ) ) : the_row(); ?>
                <?php $image = get_sub_field( 'image' ); ?>
                <?php $button = get_sub_field('button'); ?>
                <li class="<?php if(get_sub_field( 'overlay' )){echo 'has_overlay';} ?>">
                    <img class="uk-width" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" uk-cover>
                    <div class="position-center">
                        <div class="uk-container">
                            <?php $header = get_sub_field( 'header' ); ?>
                            <h2><span style="color: #F98A3C;"><?php echo trim($header[ 'orange_text' ] ); ?></span><br><?php echo trim($header[ 'white_text' ] ); ?></h2>
                            <p><?php the_sub_field('small_text'); ?></p>
                            
                            <?php $button_href = $button['button_location'] == "Internal"?get_the_permalink( $button['internal_page'] ):$button['external_page']; ?>
    
                            <a href="<?php echo $button_href; ?>" class="uk-button uk-button-small button-hover-effect" <?php if($button['button_location'] == "External"){ echo 'target="_blank"'; } ?>><?php echo $button['button_text']; ?></a>
                        </div>
                    </div>
                </li>
                <?php endwhile; ?>
            </ul>
        </div>

        <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>

    </div>

    <div class="uk-hidden@m" uk-slider>
        <ul class="uk-slider-items uk-child-width-1-1">
                <?php while ( have_rows( 'hps_slides' ) ) : the_row(); ?>
                <?php $image = get_sub_field( 'image' ); ?>
                <?php $button = get_sub_field('button'); ?>
            <li class="">
                <div class="uk-cover-container overlay <?php if(get_sub_field( 'overlay' )){echo 'has_overlay';} ?>">
                    <canvas width="1920" height="600"></canvas>
                    <img class="uk-width" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" uk-cover>
                </div>
                <div class="uk-container">
                    <?php $header = get_sub_field( 'header' ); ?>
                    <h2><span style="color: #F98A3C;"><?php echo trim($header[ 'orange_text' ] ); ?></span><br><?php echo trim($header[ 'white_text' ] ); ?></h2>
                    <p><?php the_sub_field('small_text'); ?></p>

                    <?php $button_href = $button['button_location'] == "Internal"?get_the_permalink( $button['internal_page'] ):$button['external_page']; ?>

                    <a href="<?php echo $button_href; ?>" class="uk-button uk-button-small button-hover-effect"><?php echo $button['button_text']; ?></a>
                </div>
            </li>
            <?php endwhile ?>
        </ul>

        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
    </div>


    </div>
</section>


<style>
    .home_page_slider .uk-slider.uk-hidden\@m .uk-slider-items li:first-child img{
        transform: translate(5%, -50%);
    }
</style>

<?php endif; ?>