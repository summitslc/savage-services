<?php
?>
<article id="post-<?php the_ID();?>" class="<?php post_class('uk-article uk-link-toggle');?>">
    <div class="uk-cover-container<?php if( !has_post_thumbnail() ) { echo ' bordered'; } ?>">
        <canvas width="" height=""></canvas>
        <img src="<?php echo get_the_post_thumbnail_url($page->ID, 'full')?:CHILDPATH.'/images/recent_news_placeholder.png' ?>" uk-cover>
    </div>
    <div class="post__right">
        <p class="uk-article-meta">
            <span class="post__date"><?php the_date(); ?></span>
        </p>
        <h2 class="post__title uk-article-title uk-margin-remove-bottom"><a class="uk-link-heading" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"><?php the_title();?></a></h2>
        <p class="post__excerpt"><?php the_excerpt();?></p>
        <a class="post__action btn" href="<?php the_permalink();?>">Read More</a>
    </div>
</article>
