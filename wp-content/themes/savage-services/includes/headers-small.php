<?php if( has_post_thumbnail( ) ){ ?>
    <div class="featured_image--small uk-cover-container">
        <canvas width="870" height="160"></canvas>
        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" uk-cover >
    </div>
<?php } ?>