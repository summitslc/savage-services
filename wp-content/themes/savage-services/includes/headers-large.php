<div class="featured_image--large">
    <?php the_post_thumbnail(); // Fullsize image for the single post 
    ?>
    <!-- <img src="https://picsum.photos/1000/1500" alt="" > -->
    
    <div id="header-large-content" class="uk-width-1-4 uk-align-right uk-position-center uk-visible@l">
        <h1><?php if (get_field('hero_text_title_orange')) : ?><span class="text-orange"><?php the_field('hero_text_title_orange', false, false); ?></span><br /><?php endif ?>
            <?php the_field('hero_text_title', false, false); ?></h1>
        <p><?php the_field('hero_text_body', false, false); ?></p>
    </div>
</div>
<div class="uk-hidden@l uk-padding">
    <h1><?php if (get_field('hero_text_title_orange')) : ?><span class="text-orange"><?php the_field('hero_text_title_orange', false, false); ?></span><br /><?php endif ?>
        <?php the_field('hero_text_title', false, false); ?></h1>
    <p><?php the_field('hero_text_body', false, false); ?></p>
</div>