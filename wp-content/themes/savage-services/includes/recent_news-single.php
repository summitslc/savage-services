<article id="post-<?php the_ID();?>" class="<?php post_class('uk-article uk-link-toggle');?>">
    <a class="uk-link-heading" href="<?php the_permalink();?>">
        <?php
        $thumbnail = has_post_thumbnail() ? get_the_post_thumbnail_url( ) : CHILDPATH . '/images/recent_news_placeholder.png' ;
        ?>
        <div class="uk-cover-container<?php echo has_post_thumbnail()?'':' default_img' ?>">
            <canvas></canvas>
            <img src="<?php echo $thumbnail; ?>" alt="<?php the_title(); ?>" uk-cover>
        </div>
        <header>
            <h3 class="uk-margin-remove-bottom"><?php the_title();?></h3>
        </header>
        <p class="post__excerpt"><?php the_excerpt();?></p>
    </a>
</article>