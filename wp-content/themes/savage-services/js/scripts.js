// Base Camp Custom JavaScript

(function ($, $$) {
    $(document).ready(function () {
        "use strict";

        // Implements the +/-s on the mobile nav
        $('.mobileNav .sub-menu').slideUp(0);
        $('.mobileNav .plus').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            $(this).parent().next().slideUp(750);
            $(this).removeClass('m');

            if (!$(this).parent().next().is(':visible')) {
                $(this).parent().next().slideDown(750);
                $(this).addClass('m');
            }
        });
        $(this).parent().next().slideUp(0);
    });
})(jQuery, UIkit.util, this);

(function ($, $$) {
    $(function () {
        "use strict";

        if ($('.recent_news').length) {
            $$.on(window, 'resize', window.debounce(function (e) { // debounce the event to wait for UIKIT to do its computations
                var slider = UIkit.slider('.recent_news .uk-slider');
                if (slider.maxIndex == 0 && slider.draggable) {
                    slider.draggable = false;
                } else if (slider.maxIndex > 0 && !slider.draggable) {
                    slider.draggable = true;
                }
            }, 100));
            window.dispatchEvent(new Event('resize'));
        }

    });

})(jQuery, UIkit.util, this);


window.debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};