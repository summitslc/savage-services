(function ($, $$) {
    $(document).ready(function () {
      "use strict";
      function checkScreenSize() {
        if($(window).width() > 960)  {
          $('.uk-accordion-content').attr('aria-hidden', 'false');
          $('.uk-accordion-content').removeAttr('hidden');
          $('.uk-accordion li').addClass('uk-open');
          // console.log($('.uk-accordion-content'));
        } else {
          $('.uk-accordion-content:not(:first-of-type)').attr('aria-hidden', 'true');
          $('.uk-accordion li:not(:first-of-type) .uk-accordion-content').attr('hidden', '');
          $('.uk-accordion li:not(:first-of-type)').removeClass('uk-open');
        }
      }
      checkScreenSize();

      $( window ).resize(function() {
        // $( "#log" ).append( "<div>Handler for .resize() called.</div>" );
        checkScreenSize();
      });

    });
})(jQuery, UIkit.util, this);