<?php /* Template Name: Demo */ 
get_header(); ?>

<main role="main" class="">
  <section class="uk-section">
    <div class="uk-container">
      <?php 
      if (have_posts()){
          while (have_posts()){
              the_post();
              the_content();
            }
        }
        ?>

    </div>
  </section>
</main>

<?php get_footer(); ?>