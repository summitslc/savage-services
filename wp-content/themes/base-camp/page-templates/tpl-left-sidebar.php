<?php

/**
 * Template Name: Left Side Bar
 */

get_header(); ?>

<main role="main" class="page">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <section>
                <div class="uk-container">
                    <div class="uk-grid">
                        <div class="uk-width-1-4 uk-visible@l">
                            <?php get_sidebar(); ?>
                        </div>
                        <div class="uk-width-3-4@l">
                            <?php get_template_part('includes/page', 'content'); ?>
                        </div>
                    </div>
            </section>

        <?php endwhile; ?>

    <?php else : ?>


        <article>

            <section class="uk-section">
                <div class="uk-container">
                    <p>We're sorry, but there isn't anything here.</p>
                </div>
            </section>
        </article>

    <?php endif; ?>

</main>

<?php get_footer(); ?>