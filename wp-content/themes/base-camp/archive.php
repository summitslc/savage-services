<?php

/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Base_Camp
 */
get_header(); ?>

<main class="archive" role="main">
	<div class="uk-container">
		<?php
		the_archive_title('<h1 class="page-title">', '</h1>');
		?>
		<div class="uk-grid">
			<div class="uk-section uk-width-2-3@l">
				<?php get_template_part('includes/loop'); ?>
				<?php get_template_part('includes/pagination'); ?>
			</div>
			<div class="uk-section uk-visible@l">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>