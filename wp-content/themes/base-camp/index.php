<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Base_Camp
 */

get_header(); ?>

<main role="main" class="main">

<?php esc_html_e('If you\'re reading this, then you should be using template higher up in the chain.', 'base-camp'); ?>
	
</main>

<?php get_footer(); ?>
