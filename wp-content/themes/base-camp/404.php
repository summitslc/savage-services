<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_anesc_html_error_404_Page
 *
 * @package Base_Camp
 */

get_header(); ?>

<main role="main" class="404">

	<section class="uk-section">
        <div class="uk-container">
            <h1><?php esc_html_e('Error: 404', 'base-camp'); ?></h1>
            <h2><?php esc_html_e('Page not found.', 'base-camp'); ?></h2>
            <p><?php esc_html_e('We\'re sorry, but it appears the content you\'re looking for either doesn\'t exist or has been removed.', 'base-camp'); ?></p>
            <p><?php esc_html_e('Head back, or search for what you\'re looking for below', 'base-camp'); ?> <a href="<?php echo home_url(); ?>"><?php esc_html_e('home', 'base-camp'); ?></a>?</p>
            <?php get_template_part( 'searchform' ); ?>
        </div>
	</section>

</main>

<?php get_footer(); ?>
