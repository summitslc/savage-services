<?php

/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Base_Camp
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
	return;
}
?>

<div id="comments" class="comments">

	<?php
	// You can start editing here -- including this comment!
	if (have_comments()) :
	?>
		<h2 class="comments__title">
			<?php
			$base_camp_comment_count = get_comments_number();
			if ('1' === $base_camp_comment_count) {
				printf(
					/* translators: 1: title. */
					esc_html__('One thought on &ldquo;%1$s&rdquo;', 'base-camp'),
					'<span>' . get_the_title() . '</span>'
				);
			} else {
				printf( // WPCS: XSS OK.
					/* translators: 1: comment count number, 2: title. */
					esc_html(_nx('%1$s thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $base_camp_comment_count, 'comments title', 'base-camp')),
					number_format_i18n($base_camp_comment_count),
					'<span>' . get_the_title() . '</span>'
				);
			}
			?>
		</h2><!-- .comments-title -->

		<?php the_comments_navigation(); ?>

		<ul class="uk-comment-list">
			<?php
			wp_list_comments(array(
				'style'    => 'ul',
				'callback' => 'alt_comments'
			));
			?>
		</ul><!-- .comment-list -->

		<?php
		the_comments_navigation();

		// If comments are closed and there are comments, let's leave a little note, shall we?
		if (!comments_open()) :
		?>
			<p class="no-comments"><?php esc_html_e('Comments are closed.', 'base-camp'); ?></p>
	<?php
		endif;

	endif;
	// Check for have_comments().
	//Setting it up this way to make it more i18n friendly
	$comment_send = 'Submit';
	$comment_reply = 'Leave a Message';
	$comment_reply_to = 'Reply';

	$comment_author = 'Name';
	$comment_email = 'E-Mail';
	$comment_body = 'Comment';
	$comment_url = 'Website';
	$comment_cookies_1 = ' By commenting you accept the';
	$comment_cookies_2 = ' Privacy Policy';

	$comment_before = 'Registration isn\'t required.';

	$comment_cancel = 'Cancel Reply';

	//Array
	$comments_args = array(
		//Define Fields
		'fields' => array(
			//Author field
			'author' => '<p class="comment-form-author"><br /><input id="author" name="author" class="uk-input" name="author" type="text" aria-required="true" placeholder="' . $comment_author . '"></p>',
			//Email Field
			'email' => '<p class="comment-form-email"><br /><input id="email" class="uk-input" name="email" type="email" placeholder="' . $comment_email . '"></p>',
			//URL Field
			'url' => '<p class="comment-form-url"><br /><input id="url" name="url" class="uk-input" type="url" placeholder="' . $comment_url . '"></p>',
			//Cookies
			'cookies' => '<input id="privacy" name="privacy" class="uk-checkbox" type="checkbox" required>' . $comment_cookies_1 . '<a href="' . get_privacy_policy_url() . '">' . $comment_cookies_2 . '</a>',
		),
		// Change the title of send button
		'label_submit' => __($comment_send, 'base-camp'),
		// Change the title of the reply section
		'title_reply' => __($comment_reply, 'base-camp'),
		// Change the title of the reply section
		'title_reply_to' => __($comment_reply_to, 'base-camp'),
		//Cancel Reply Text
		'cancel_reply_link' => __($comment_cancel, 'base-camp'),
		// Redefine your own textarea (the comment body).
		'comment_field' => '<p class="comment-form-comment"><br /><textarea id="comment" class="uk-textarea" name="comment" aria-required="true" placeholder="' . $comment_body . '"></textarea></p>',
		//Message Before Comment
		'comment_notes_before' => __($comment_before, 'base-camp'),
		// Remove "Text or HTML to be displayed after the set of comment fields".
		'comment_notes_after' => '',
		//Submit Button ID
		'id_submit' => __('comment-submit', 'base-camp'),
		'class_submit' => 'uk-input'
	);

	comment_form($comments_args);
	?>

</div><!-- #comments -->