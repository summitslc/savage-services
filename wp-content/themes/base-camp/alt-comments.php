<?php
// My custom comments output html
// Based on: https://www.wpexplorer.com/better-wordpress-comments/
function alt_comments($comment, $args, $depth)
{

    // Get correct tag used for the comments
    if ('div' === $args['style']) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    } ?>


    <<?php echo $tag; ?> class="uk-comment" id="comment-<?php comment_ID() ?>">

        <?php
        // Switch between different comment types
        switch ($comment->comment_type):
            case 'pingback':
            case 'trackback': ?>
                <div class="pingback-entry"><span class="pingback-heading"><?php esc_html_e('Pingback:', 'base-camp'); ?></span> <?php comment_author_link(); ?></div>
                <?php
                break;
            default:

                if ('div' != $args['style']) { ?>

                    <div id="div-comment-<?php comment_ID() ?>" class="uk-comment">
                        <header class="uk-comment-header">
                            <div class="uk-grid-medium uk-flex-middle" uk-grid>
                            <?php } ?>
                            <div class="uk-width-auto">
                                <?php
                                // Display avatar unless size is set to 0
                                if ($args['avatar_size'] != 0) {
                                    $avatar_size = !empty($args['avatar_size']) ? $args['avatar_size'] : 70; // set default avatar size
                                    echo '<img src="' . esc_url(get_avatar_url($comment)) . '" width="' . $avatar_size . '" alt' . 'class="uk-comment-avatar"' . '/>';
                                }
                                // Display author name
                                ?>
                            </div>
                            <div class="uk-width-expand">
                                <?php
                                echo '<h4 class="uk-comment-title uk-margin-remove">';
                                echo '<a class="uk-link-reset" href="' . get_author_posts_url($comment) . '">' . get_comment_author($comment) . '</a>';
                                echo '</h4>' ?>
                                <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                                    <li><?php echo get_comment_date() . ' ' . get_comment_time() ?></li>
                                    <li>
                                        <?php
                                        // Display comment reply link
                                        comment_reply_link(array_merge($args, array(
                                            'add_below' => $add_below,
                                            'depth'     => $depth,
                                            'max_depth' => $args['max_depth']
                                        ))); ?>
                                    </li>
                                    <?php if (get_edit_comment_link($comment)) : // check if user can edit
                                    ?>
                                        <li>
                                            <a href="<?php echo get_edit_comment_link($comment) ?>"><?php esc_html_e('Edit', 'base-camp') ?></a>
                                        </li>
                                    <?php endif ?>

                                </ul>
                                <!-- <div class="comment-meta commentmetadata">
                                            
                                        </div>.comment-meta -->
                            </div>
                            </div>
                        </header>

                        <!-- .comment-author -->
                        <div class="uk-comment-body">
                            <?php comment_text(); ?>
                            <!-- .comment-text -->
                            <?php
                            // Display comment moderation text
                            if ($comment->comment_approved == '0') { ?>
                                <em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'base-camp'); ?></em><br /><?php
                                                                                                                                                } ?>

                        </div><!-- .comment-details -->
                        <?php
                        if ('div' != $args['style']) { ?>
                    </div>
    <?php }
                        // IMPORTANT: Note that we do NOT close the opening tag, WordPress does this for us
                        break;
                endswitch; // End comment_type check.
            }
