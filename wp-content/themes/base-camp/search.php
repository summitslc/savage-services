<?php

/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Base_Camp
 */

get_header(); ?>

<main role="main" class="search">
  <div class="uk-container">
    <h1 class="page-title">
      <?php esc_html_e('Search results for:', 'basecamp'); ?> <?php echo get_search_query(); ?>
    </h1>
    <div class="uk-grid">
      <div class="uk-section uk-width-3-4@l">
        <?php get_template_part('includes/loop'); ?>
        <?php get_template_part('includes/pagination'); ?>
      </div>
      <div class="uk-section uk-width-1-4 uk-visible@l">
        <?php get_sidebar(); ?>
      </div>
    </div>
  </div>

</main>

<?php get_footer(); ?>