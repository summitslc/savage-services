<!-- search -->
<form class="search uk-search uk-search-default uk-width-1-1" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="uk-search-input uk-input-small" type="search" name="s" placeholder="<?php  esc_html_e('Search...', 'base-camp'); ?>">
	<button class="search-submit uk-search-icon-flip" type="submit" uk-search-icon></button>
</form>
<!-- /search -->
