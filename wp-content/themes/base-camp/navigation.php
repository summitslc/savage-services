<header class="header" role="banner">
  <?php // Line 3 is an example of how to make the navbar sticky with some extra features. To Use add </div> after the </nav> 
  ?>
  <!-- <div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky shadowy; cls-inactive: uk-animation-reverse <?php if (is_front_page()) echo 'uk-navbar-transparent uk-position-top hidemenu'; ?>; top: #body_menu"> -->
  <div class="uk-container">
    <div uk-grid class="uk-grid uk-grid-small">
      <div class="uk-width-expand">
        <nav class="uk-container" uk-navbar>
          <div class="uk-navbar-left">
            <a class="uk-navbar-item uk-logo" href="<?php echo home_url(); ?>">
              <img style="max-height: 100px;" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Logo" class="header__logo">
            </a>
          </div>
          <div class="uk-navbar-left">
            <?php the_field( 'header_rol', 'header' ); ?>
          </div>
          <div class="uk-navbar-right">
            <a id="mobile_hamburger" class="uk-navbar-toggle uk-hidden@m" uk-toggle="target: #mobile_nav">
              <span uk-icon="icon: menu; ratio: 2;" class="uk-animation-fade"></span>
              <span uk-icon="icon: close; ratio: 2;" class="uk-animation-fade"></span>
            </a>
            <?php /* shows in admin as "Primary" */ if (has_nav_menu('menu-1')) wp_nav_menu(array('theme_location' => 'menu-1', 'menu_id' => 'main_menu', 'menu_class' => 'uk-visible@m uk-navbar-nav uk-nav')); ?>
            
          </div>
        </nav>
      </div>
      <div class="uk-width-1-6 uk-visible@m uk-navbar-right">
        <?php get_template_part('searchform'); ?>
      </div>
    </div>
    <div><?php if (has_nav_menu('menu-2')) : ?></div>
  </div>

  <nav class="uk-container" uk-navbar>
    <div class="uk-nav-bar uk-navbar-center">
      <?php /* shows in admin as "Secondary" */ if (has_nav_menu('menu-2')) wp_nav_menu(array('theme_location' => 'menu-2', 'menu_id' => 'main_menu', 'menu_class' => 'uk-visible@m uk-navbar-nav uk-nav')); ?>
    </div>
  </nav>
<?php endif; ?>

<div id="mobile_nav" uk-offcanvas="flip: true; overlay: true; mode: reveal;" class="uk-offcanvas uk-hidden@m">
  <div class="uk-offcanvas-bar uk-flex uk-flex-column">
    <?php /* shows in admin as "Primary" */ if (has_nav_menu('menu-1')) wp_nav_menu(array('theme_location' => 'menu-1', 'menu_class' => "uk-nav-center uk-nav")); ?>
    <?php /* shows in admin as "Secondary" */ if (has_nav_menu('menu-2')) wp_nav_menu(array('theme_location' => 'menu-2', 'menu_class' => "uk-nav-center uk-nav")); ?>
    <?php get_template_part('searchform'); ?>
  </div>
</div>
<div class="uk-container uk-padding-small breadcrumbs_cont"><?php custom_breadcrumbs(); ?></div>
</header>