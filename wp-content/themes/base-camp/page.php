<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Base_Camp
 */

get_header(); ?>

<main role="main" class="page">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<section class="uk-section">
				<div class="uk-container">
				<?php get_template_part('includes/page' , 'content'); ?>
				</div>
			</section>

		<?php endwhile; ?>

	<?php else : ?>


		<article class="not-found">

			<section class="uk-section">
				<div class="uk-container">
					<p><?php esc_html_e('We\'re sorry, but there isn\'t anything here.', 'base-camp'); ?></p>
				</div>
			</section>
		</article>

	<?php endif; ?>

</main>

<?php get_footer(); ?>