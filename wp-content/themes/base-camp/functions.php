<?php
/**
 * Base Camp functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Base_Camp
 */

 
define('PARENTPATH', get_template_directory_uri());
define('CHILDPATH', get_stylesheet_directory_uri());
 
if ( ! function_exists( 'base_camp_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function base_camp_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Base Camp, use a find and replace
		 * to change 'base-camp' to the name of your theme in all the template files.
		 */

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size('small', 120, '', true); // Small Thumbnail
		add_image_size('medium', 300, '', true); // Medium Thumbnail
		add_image_size('large', 600, '', true); // Large Thumbnail
		add_image_size('hd', 1920, 1080, true); // hd
		add_image_size('sd', 1280, 720, true); // sd
		add_image_size('custom-size', 700, '250', true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'base-camp' ),
			'menu-2' => esc_html__( 'Secondary', 'base-camp' )
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add theme support for Menus
		add_theme_support('menus');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'base_camp_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function base_camp_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'base_camp_content_width', 640 );
}
add_action( 'after_setup_theme', 'base_camp_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function base_camp_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'base-camp' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'base-camp' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'base_camp_widgets_init' );

/* ========================================
	Actions
======================================== */

// ADD Actions
function my_login_logo() { // Replace the WP Login Screen logo with TSG ?>
	<style type="text/css">
			#login h1 a, .login h1 a {
				background-image: url(<?php echo get_template_directory_uri(); ?>/images/login-logo.png);
				height:200px;
				width:200px;
				background-size: 200px 200px;
				background-repeat: no-repeat;
			}
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() { // Replace WP logo with TSG URL
	return 'https://www.summitslc.com/';
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() { // Replace title attr on WP logo
	return 'Powered by The Summit Group';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

function fb_disable_feed() { // Disable RSS features
	wp_die( __('Sorry, RSS has been disabled on this site.') );
}
add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);

// REMOVE Actions
remove_action('welcome_panel', 'wp_welcome_panel'); // Removes the Welcome Panel from the dashboard.
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator'); // Removes the meta tag telling what version of WordPress is being used.
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7); //Noone uses emojis in their blogs, stop including them
remove_action('wp_print_styles', 'print_emoji_styles'); //stop including emojies

/* ========================================
	Filters
======================================== */

// ADD Filters
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)

add_filter('edit_post_link', 'change_default_edit_btn_text', 10, 3); // Add uikit defaults to button so it doesn't look like poop
function change_default_edit_btn_text($link, $id, $text, $class = "uk-button uk-button-default"){
		return substr_replace($link, 'uk-button uk-button-default ', strpos($link, 'post-edit-link'), 0);
}

function remove_category_rel_from_category_list($thelist)
{
  return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute

add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)

function remove_admin_bar()
{
  return false;
}
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar

function remove_thumbnail_dimensions( $html )
{
	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	return $html;
}
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

function no_wordpress_errors(){
	return 'The credenitals you provided are invalid or were input incorrectly.';
}
add_filter( 'login_errors', 'no_wordpress_errors' ); // Changed the error message on wrong input to login page.

add_filter( 'gform_confirmation_anchor', '__return_true' ); //Gravity form confirmation anhor

// REMOVE Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether


/* ========================================
	Enqueue scripts and styles
======================================== */

function base_camp_scripts() {
	
	wp_enqueue_script( 'jquery' );
	
	
	// wp_enqueue_style( 'uikit-style', PARENTPATH . '/styles/base/uikit.min.css' );
	wp_enqueue_script( 'uikit-js', PARENTPATH . '/js/uikit.min.js', array(), '20151215', false );
	wp_enqueue_script( 'uikit-icons', PARENTPATH . '/js/uikit-icons.min.js', array(), '20151215', false );
	
	wp_enqueue_script( 'base-camp-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	
	wp_enqueue_script( 'base-camp-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	// Adds on page validation to comments form
    wp_enqueue_script('jquery-validate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js', array(), '', false);
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	/* THESE SHOULD BE LAST SO THEY GET HIGHEST PRIORITY */
	wp_enqueue_style( 'base-camp-style', get_stylesheet_uri() );
	wp_enqueue_style( 'base-camp-style-custom', get_template_directory_uri() . '/style.min.css', array(), '0.0.1' );
	wp_enqueue_script( 'base-camp-scripts', get_template_directory_uri() . '/js/scripts.js', array(), '0.0.1', true );
}
add_action( 'wp_enqueue_scripts', 'base_camp_scripts' );

/* ========================================
	Pagination
======================================== */

function base_camp_pagination() {
	global $wp_query;
	$big = 999999999;
	echo paginate_links( array(
		'base'		=> str_replace($big, '%#%', get_pagenum_link($big)),
		'format'	=> '?paged=%#%',
		'current'	=> max(1, get_query_var('paged')),
		'total'		=> $wp_query->max_num_pages
	));
}
add_action('init', 'base_camp_pagination');
 
 
/* ========================================
	ACF Custom Options Pages
======================================== */

if( function_exists('acf_add_options_page') ) {
   
    acf_add_options_page(array( /* adds option page to admin sidebar */
        'page_title' 	=> 'Theme Settings', 
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-settings',
        'capability'	=> 'edit_posts', /* who should see it */
        'redirect'		=> true /* true goes to first child, false goes to it's own page */
    ));
 
    acf_add_options_page(array(
        'page_title' 	=> 'Social Icons',
        'menu_title'	=> 'Social Icons',
        'menu_slug' 	=> 'socialicons',
        'capability'	=> 'edit_posts',
        'post_id'       => 'socialicons', /* changing post ID allows multiple "header" acf fields, use {post_id} instead of 'option' e.g: `get_field('header', 'subpage')` instead of default `get_field('header', 'option') */
        'parent_slug' 	=> 'theme-settings', /* parent slug adds this as a child menu item, redirect true will go to this page */
    ));
	
	acf_add_options_page(array(
        'page_title' 	=> 'Header',
        'menu_title'	=> 'Header',
        'menu_slug' 	=> 'header',
        'capability'	=> 'edit_posts',
        'post_id'       => 'header', /* changing post ID allows multiple "header" acf fields, use {post_id} instead of 'option' e.g: `get_field('header', 'subpage')` instead of default `get_field('header', 'option') */
        'parent_slug' 	=> 'theme-settings', /* parent slug adds this as a child menu item, redirect true will go to this page */
    ));
	
	acf_add_options_page(array(
        'page_title' 	=> 'Footer',
        'menu_title'	=> 'Footer',
        'menu_slug' 	=> 'footer',
        'capability'	=> 'edit_posts',
        'post_id'       => 'footer', /* changing post ID allows multiple "header" acf fields, use {post_id} instead of 'option' e.g: `get_field('header', 'subpage')` instead of default `get_field('header', 'option') */
        'parent_slug' 	=> 'theme-settings', /* parent slug adds this as a child menu item, redirect true will go to this page */
    ));
     
 }

/* ========================================
	Custom Post Types: https://generatewp.com/post-type/ is a good tool for getting started
======================================== */

/* ========================================
	Excerpt Options
======================================== */

function isacustom_excerpt_length($length) {
	global $post;
	if ($post->post_type == 'post')
	return 20;
	else
	return 50;
}
add_filter('excerpt_length', 'isacustom_excerpt_length', 999);

/* ========================================
	Remove the <div> surrounding the 
	dynamic navigation to cleanup markup
======================================== */

function my_wp_nav_menu_args($args = '') {
	$args['container'] = false;
	return $args;
}
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args');


/* ========================================
	Cleanup the default wordpress navigation classes
	to make it easier to read/use
======================================== */
// add_filter('nav_menu_css_class' , 'uikit_nav_class' , 10 , 2);

function uikit_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes) ){
        $classes[] = 'uk-active ';
    }
    return $classes;
}

// add_filter('nav_menu_submenu_css_class' , 'uikit_sub_nav_class' , 10 , 2);

function uikit_sub_nav_class ($classes, $item) {
    if (in_array('sub-menu', $classes) ){
        $classes[] = 'uk-nav uk-nav-sub';
    }
    return $classes;
}

/* ========================================
	Updated Comments template
======================================== */
require_once get_parent_theme_file_path( '/alt-comments.php' );

/* ========================================
	Add page slug to body class
======================================== */

function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }
    return $classes;
}
add_filter('body_class', 'add_slug_to_body_class');

/* ========================================
	Load More Button (AJAX)
======================================== */

function misha_loadmore_ajax_handler(){
	global $wp_query;
	 // prepare our arguments for the query
	 $args = json_decode( stripslashes( $_POST['query'] ), true );
	 $args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	 $args['post_status'] = 'publish';
	
	 // it is always better to use WP_Query but not here
		 query_posts( $args );
 
		 get_template_part('loop');
		 
	 die; // here we exit the script and even no wp_reset_query() required!
 }
 
 add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
 add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

 /* ========================================
	On page Comment Validation
======================================== */

// Reference: https://learnersbucket.com/examples/wordpress/comment-form-validation-in-wordpress/
/*Comment Validation*/
function comment_validation_init()
{
	if (is_single() && comments_open()) { ?>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('#commentform').validate({
					rules: {
						author: {
							required: true,
							minlength: 2
						},
						email: {
							required: true,
							email: true
						},
						comment: {
							required: true,
							minlength: 2
						},
						url: {
							required: false,
							minlength: 0
						},
						privacy: {
							required: true
						}
					},

					messages: {
						author: "<?php esc_html_e('Please provide a name', 'base-camp') ?>",
						email: "<?php esc_html_e('Please enter a valid email address.', 'base-camp') ?>",
						comment: "<?php esc_html_e('Please fill the required field.', 'base-camp') ?>",
						url: "<?php esc_html_e('Please enter a valid URL.', 'base-camp') ?>",
						privacy: "<?php esc_html_e('Please read and accept our privacy policy', 'base-camp') ?>"
					},

					errorClass: "uk-form-danger",
					errorElement: "div",
					errorPlacement: function(error, element) {
						element.after(error);
						console.log(error);
					}

				});
			});
		</script>
<?php
	}
}
add_action('wp_footer', 'comment_validation_init');

function remove_h1_option_from_tinymce_editor($args) {
    // Just omit h1 from the list
    $args['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;';
    return $args;
}
add_filter('tiny_mce_before_init', 'remove_h1_option_from_tinymce_editor' );

/* ========================================
	Included files
======================================== */
	//Move the other included files here after everything gets merged into Master and possibly move this higher on the page?
	include('breadcrumbs.php');
    // Default Social Icons
    include('default_acf_fields.php');
    

?>