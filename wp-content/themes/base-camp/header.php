<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Base_Camp
 */

?>
<!doctype html>
<html <?php language_attributes();?>>

<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php (is_front_page()) ? bloginfo('name') : wp_title('');?></title>

	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="//www.google-analytics.com" rel="dns-prefetch">
  	<link href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" rel="shortcut icon">

	<?php get_template_part('includes/google-analytics'); ?>
	
	<?php wp_head();?>
</head>

<body <?php body_class();?>>


	<?php get_template_part( 'navigation' ); ?>
