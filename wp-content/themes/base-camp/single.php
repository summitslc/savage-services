<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Base_Camp
 */

get_header(); ?>

<main role="main" class="post uk-container">
  <div class="uk-grid">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class('uk-section uk-width-3-4@l'); ?>>
          <div>
            <div class="uk-container">

              <div class="post__thumbnail">
                <?php if (has_post_thumbnail()) : // Check if Thumbnail exists 
                ?>
                  <?php the_post_thumbnail(); // Fullsize image for the single post 
                  ?>
                <?php endif; ?>
              </div>

              <h1 class="uk-article-title post__title">
                <?php the_title(); ?>
              </h1>

              <div class="uk-article-meta uk-padding-small uk-padding-remove-horizontal post__meta">
                <?php get_template_part('includes/post-meta'); ?>
              </div>

              <div class="post__content">
                <?php the_content(); ?>
              </div>

              <div class="uk-padding-small uk-padding-remove-horizontal">

                <div class="post__tags">
                  <span>Tags:
                    <?php $post_tags = get_the_tags();

                    if ($post_tags) {
                      $tag_count = 0;
                      $tag_length = count($post_tags);
                      foreach ($post_tags as $tag) {
                        // Keeps it from putting a comma after the last item in the list
                        if ($tag_count != $tag_length - 1) {
                          echo '<a href="' . get_tag_link($tag) . '">' . $tag->name . '</a>, ';
                        } else {
                          echo '<a href="' . get_tag_link($tag) . '">' . $tag->name . '</a> ';
                        }
                        $tag_count++;
                      }
                    } ?>
                  </span>
                </div>

                <div class="post__category">
                  <span>Categorised in:
                    <?php
                    $taxonomy = 'category';

                    // Get the term IDs assigned to post.
                    $post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'ids'));

                    // Separator between links.
                    $separator = ', ';

                    if (!empty($post_terms) && !is_wp_error($post_terms)) {

                      $term_ids = implode(',', $post_terms);

                      $terms = wp_list_categories(array(
                        'title_li' => '',
                        'style'    => 'none',
                        'echo'     => false,
                        'taxonomy' => $taxonomy,
                        'include'  => $term_ids
                      ));

                      $terms = rtrim(trim(str_replace('<br />',  $separator, $terms)), $separator);

                      // Display post categories.
                      echo  $terms;
                    } ?>
                  </span>
                </div>
              </div>
              <?php if (get_edit_post_link()) : ?>
                <div class="uk-padding-small uk-padding-remove-horizontal post__edit">
                  <?php edit_post_link(); ?>
                </div>
              <?php endif ?>

              <div class="uk-padding-small uk-padding-remove-horizontal post__comments">
                <?php comments_template(); ?>
              </div>

            </div>
          </div>
        </article>
      <?php endwhile; ?>

    <?php else : ?>

      <article class="not-found">
        <div class="uk-section-default">
          <div class="uk-container">
            <p><?php esc_html_e('We\'re sorry, but there isn\'t anything here.', 'base-camp'); ?></p>
          </div>
        </div>
      </article>

    <?php endif; ?>
    <div class="uk-section uk-width-1-4 uk-visible@l">
      <?php get_sidebar(); ?>
    </div>
  </div>
</main>

<?php get_footer(); ?>
