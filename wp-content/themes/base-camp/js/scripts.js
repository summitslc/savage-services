// Base Camp Custom JavaScript

(function($, $$){
  $(function(){
    "use strict";
    /* ========================================
      Default mobile menu behavior
    ======================================== */
    $$.on("#mobile_nav", "beforeshow beforehide", function(e) {
      if (e.type == "beforeshow") {
        $$.addClass($$.$("#mobile_hamburger"), "mobile_open");
      }
      if (e.type == "beforehide") {
        $$.removeClass($$.$("#mobile_hamburger"), "mobile_open");
      }
    });
    
    
    /* ========================================
      IE Hacks
    ======================================== */
    // Removes the jumpy nature of fixed background images
    // if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
		// 	$('body').on("mousewheel", function (event) {
		// 			// remove default behavior
		// 			event.preventDefault();
	
		// 			//scroll without smoothing
		// 			var wheelDelta = event.originalEvent.wheelDelta;
		// 			var currentScrollPosition = window.pageYOffset;
		// 			window.scrollTo(0, currentScrollPosition - wheelDelta);
		// 	});
	  // }  
    
    /* ========================================
      Load More Button (AJAX)
    ======================================== */
    // use jQuery code inside this to avoid "$ is not defined" error
    $(".misha_loadmore").click(function() { 
      var button = $(this),
        data = {
          action: "loadmore",
          query: misha_loadmore_params.posts, // that's how we get params from wp_localize_script() function
          page: misha_loadmore_params.current_page
        },
        prevText = button.text();

      $.ajax({
        // you can also use $.post here
        url: misha_loadmore_params.ajaxurl, // AJAX handler
        data: data,
        type: "POST",
        beforeSend: function(xhr) {
          button.text("Loading Posts").addClass('loading'); // change the button text, you can also add a preloader image
        },
        success: function(data) {
          if (data) {
            button.text(prevText).removeClass('loading').before(data); // insert new posts
            misha_loadmore_params.current_page++;

            if ( misha_loadmore_params.current_page == misha_loadmore_params.max_page )
              button.remove(); // if last page, remove the button

          } else {
            button.remove(); // if no data, remove the button as well
          }
        }
      });
    });

  });
})(jQuery, UIkit.util, this);