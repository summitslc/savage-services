<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Base_Camp
 */

?>

<footer class="footer uk-section-xsmall uk-text-center" role="contentinfo">
    <?php if (get_field('footer', 'footer')) : ?>
        <div class="uk-container uk-padding-small">
            <?php the_field('footer', 'footer', false, false); ?>
        </div>
    <?php endif ?>
  <div class="uk-container">
    <?php if (has_nav_menu('menu-3')): ?>
    <nav class="uk-container uk-navbar-container " uk-navbar>
      <div class="uk-navbar-center">
        <?php /* shows in admin as "Tertiary" - update this to secondary if we don't want the extra menu in the header */ wp_nav_menu(array('theme_location' => 'menu-3', 'menu_id' => 'main_menu', 'menu_class' => 'uk-navbar-nav uk-nav')); ?>
      </div>
    </nav>
    <?php endif ?>
    <div class="uk-container footer__copyright">
    <?php echo get_bloginfo('name') ?>&copy; <?php echo date('Y'); ?> <?php esc_html_e('Copyright.', 'base-camp'); ?>.
    </div>
    <div class="uk-container">
      <?php if (get_field('facebook_url', 'socialicons')) : ?>
          <a href="<?php the_field('facebook_url', 'socialicons'); ?>"><?php the_field('facebook_icon', 'socialicons', false, false); ?></a>
      <?php endif ?>
      <?php if (get_field('twitter_url', 'socialicons')) : ?>
        <a href="<?php the_field('twitter_url', 'socialicons'); ?>"><?php the_field('twitter_icon', 'socialicons', false, false); ?></a>
      <?php endif ?>
      <?php if (get_field('youtube_url', 'socialicons')) : ?>
        <a href="<?php the_field('youtube_url', 'socialicons'); ?>"><?php the_field('youtube_icon', 'socialicons', false, false); ?></a>
      <?php endif ?>
      <?php if (get_field('instagram_url', 'socialicons')) : ?>
        <a href="<?php the_field('instagram_url', 'socialicons'); ?>"><?php the_field('instagram_icon', 'socialicons', false, false); ?></a>
      <?php endif ?>
      <?php if (get_field('linkedin_url', 'socialicons')) : ?>
        <a href="<?php the_field('linkedin_url', 'socialicons'); ?>"><?php the_field('linkedin_icon', 'socialicons', false, false); ?></a>
      <?php endif ?>
      <?php if (get_field('pinterest_url', 'socialicons')) : ?>
        <a href="<?php the_field('pinterest_url', 'socialicons'); ?>"><?php the_field('pinterest_icon', 'socialicons', false, false); ?></a>
      <?php endif ?>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>