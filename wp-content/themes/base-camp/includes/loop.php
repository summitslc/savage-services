<?php if (have_posts()): while (have_posts()) : the_post(); ?>

    <div class="base_loop">
        <!-- article -->
        <?php get_template_part( 'includes/post'  , 'single' ); ?>
        <!-- /article -->
    </div>

<?php endwhile; ?>

<?php else: ?>

	<article>
		<h2><?php esc_html_e('Sorry, but there isn\'t any content here', 'base-camp'); ?></h2>
	</article>

<?php endif; ?>
