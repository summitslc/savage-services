<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if (has_post_thumbnail()) : // Check if Thumbnail exists 
        ?>
    <div class="page__thumbnail">
            <?php the_post_thumbnail(); // Fullsize image for the single post 
            ?>
    </div>
        <?php endif; ?>

    <h1 class="uk-article-title page__title">
        <?php the_title(); ?>
    </h1>

    <div class="uk-article-meta uk-padding-small uk-padding-remove-horizontal post__meta">
        <?php get_template_part('includes/post-meta'); ?>
    </div>

    <div class="page__content">
        <?php the_content(); ?>
    </div>
    
    <?php if (get_edit_post_link()) : ?>
    <div class="page__edit">
        <?php edit_post_link(); ?>
    </div>
    <?php endif ?>

    <div class="page__comments">
        <?php comments_template(); ?>
    </div>
</article>