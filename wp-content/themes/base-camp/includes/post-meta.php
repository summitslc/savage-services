<span class="post__meta-date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
<span class="post__meta-author"><?php esc_html_e('Published by:', 'base-camp'); ?> <?php the_author_posts_link(); ?></span>
<span class="post__meta-comments">
    <?php if (get_comments_number() > 0) : ?>
        <?php esc_html_e('Comments:', 'base-camp'); ?> <?php echo get_comments_number(); ?>
    <?php endif; ?>
</span>