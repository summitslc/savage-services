
<script>
  (function (i, s, o, g, r, a, m) {
  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  <?php if(GA_CLIENT != ''){ ?>
	// Creates the first tracker for the client account.
	ga('create', <?php echo GA_CLIENT; ?>, 'auto');

  // Sends event to client tracker account.
  ga('send', 'pageview');
  <?php } ?>
  
  <?php if(GA_TSG != ''){ ?>
    // Creates the second tracker for the tsg account
    ga('create', <?php echo GA_TSG; ?>, 'auto', 'tsg');
    // Sends event to TSG tracker account.
    ga('tsg.send', 'pageview');
  <?php } ?>
</script>
