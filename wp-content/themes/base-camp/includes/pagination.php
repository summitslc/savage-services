<?php



$pagination = paginate_links(
    array(
        'type' => 'array',
        'mid_size' => 5,
        'prev_next' => false,
    )
);


if ($pagination){

    
    $returner = array();
    $returner[] = '<ul class="uk-pagination uk-flex-center">';
    $returner[] = '<li class="uk-pagination-previous">' . get_previous_posts_link('<span uk-pagination-previous></span>') . '</li>';
    
    for ($i = 0; $i < sizeof($pagination); $i++) {
        $returner[] = '<li class="uk-visible@s">' . $pagination[$i] . '</li>';
    }
    
    $returner[] = '<li class="uk-pagination-next">' . get_next_posts_link('<span uk-pagination-next></span>') . '</li>';
    $returner[] = '</ul>';
    
    echo implode('', $returner);
}

?>