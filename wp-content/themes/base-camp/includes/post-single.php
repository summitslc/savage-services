<?php
?>
<article id="post-<?php the_ID();?>" class="<?php post_class('uk-article uk-link-toggle');?>">
    <header>
        <h2 class="uk-article-title uk-margin-remove-bottom"><a class="uk-link-heading" href="<?php the_permalink();?>" title="<?php the_title_attribute();?>"><?php the_title();?></a></h2>
    </header>
    <?php echo get_the_post_thumbnail($page->ID, 'full'); ?>
    <div>
        <p class="uk-article-meta">
            Written by <span class="post__author" class="uk-link-heading"><?php the_author_link(); ?></a></span> on <span class="post__date"><?php the_date(); ?></span>
        </p>
        <p class="post__excerpt"><?php the_excerpt();?></p>
        <p><a href="<?php the_permalink();?>">Read More</a></p>
    </div>
</article>
<hr>
