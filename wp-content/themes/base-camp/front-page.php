<?php // WordPress recoginzes this as the index page of a site.
get_header();?>
<main role="main" class="home">
    <section class="uk-section uk-padding-remove">
        <div class="uk-container">
            <!-- <div class=""> -->
            <div class="uk-position-relative uk-light" uk-slideshow="autoplay: true; ratio: 7:3;">
                <ul class="uk-slideshow-items">
                    <li>
                        <img src="https://picsum.photos/g/1280/515/?random&v=1" alt="" uk-cover>
                        <div class="uk-text-center uk-overlay uk-overlay-primary uk-transition-fade uk-transform-origin-center-center uk-position-center uk-position-medium uk-flex uk-flex-center uk-flex-middle ">
                            <div class="uk-padding-large">
                                <h3 class="uk-margin-remove">Slide Header</h3>
                                <p class="uk-margin-remove">your content</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="https://picsum.photos/g/1280/515/?random&v=2" alt="" uk-cover>
                        <div class="uk-text-center uk-overlay uk-overlay-primary uk-transition-fade uk-transform-origin-center-center uk-position-center uk-position-medium uk-flex uk-flex-center uk-flex-middle ">
                            <div class="uk-padding-large">
                                <h3 class="uk-margin-remove">Slide Header</h3>
                                <p class="uk-margin-remove">your content</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <img src="https://picsum.photos/g/1280/515/?random&v=3" alt="" uk-cover>
                        <div class="uk-text-center uk-overlay uk-overlay-primary uk-transition-fade uk-transform-origin-center-center uk-position-center uk-position-medium uk-flex uk-flex-center uk-flex-middle ">
                            <div class="uk-padding-large">
                                <h3 class="uk-margin-remove">Slide Header</h3>
                                <p class="uk-margin-remove">your content</p>
                            </div>
                        </div>
                    </li>
                </ul>
                <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

            </div>
        </div>
    </section>



</main>

<?php get_footer();?>