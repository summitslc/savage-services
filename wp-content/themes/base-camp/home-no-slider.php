<?php get_header(); ?>

<main role="main" class="">
    <div class="uk-container">
        <h1><?php single_post_title(); ?></h1>
        <div class="uk-grid">
            <div class="uk-section uk-width-2-3@l">
                <?php get_template_part('includes/loop'); ?>
                <?php get_template_part('includes/pagination'); ?>
            </div>
            <div class="uk-section uk-visible@l">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>