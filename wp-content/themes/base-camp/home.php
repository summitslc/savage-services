<?php

/**
 * Blog posts index page
 */
/*Swap this with the code in home-no-slider.php  if you don't want a slider */

get_header(); ?>

<main role="main" class="main">
  <div>
    <div class="uk-container">
      <h1><?php single_post_title(); ?></h1>
      <?php
      // Query for slider
      $the_query = new WP_Query(array(
        'category_name' => 'uncategorized',
        'posts_per_page' => 3,
      ));

      // The rest of the posts outside the slider
      if (get_query_var('paged')) {
        $paged2 = get_query_var('paged');
      } elseif (get_query_var('page')) { // 'page' is used instead of 'paged' on Static Front Page
        $paged2 = get_query_var('page');
      } else {
        $paged2 = 1;
      }

      /* Offset needs to be set in functions.php as well to get around the confilct between offset and pagination */
      // The args for the second query
      $args2 = array(
        'paged'          => $paged2,
        'offset'         => 3,
        'post_status' => 'publish',
        'order' => 'DESC', // 'ASC'
        'orderby' => 'date' // modified | title | name | ID | rand
      );

      $query2 = new WP_Query($args2);

      ?>
      <?php if (have_posts()) : ?>
        <?php if ($the_query->have_posts()) : ?>

          <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slider="center: true">
            <div class="uk-slider-container">
              <h3>Latest</h3>
              <ul class="uk-slider-items uk-grid">

                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                  <li class="uk-width-2-3@m">
                    <div class="uk-panel">
                      <article id="post-<?php the_ID(); ?>" class="post uk-grid">
                        <div class="uk-width-1-3@m">
                          <?php echo get_the_post_thumbnail($page->ID, 'thumbnail', array('class' => 'uk-align-right uk-visible@m')); ?>
                          <?php echo get_the_post_thumbnail($page->ID, 'full', array('class' => 'uk-hidden@m')); ?>
                        </div>
                        <div class="uk-width-2-3@m">
                          <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                          <span class="post__date"><?php echo get_the_date(); ?></span>
                          <span class="post__excerpt"><?php the_excerpt(); ?></span>
                          <p><a href="<?php the_permalink(); ?>">Read More</a></p>
                        </div>
                      </article>
                    </div>
                  </li>
                <?php endwhile; ?>
              </ul>
            <?php endif; ?>
            </div>
            <a class="uk-position-center-left-out uk-position-small uk-icon uk-slidenav-previous uk-visible@m " href="#" uk-slider-item="previous"><span uk-icon=" icon: chevron-left"></span></a>
            <a class="uk-position-center-right-out uk-position-small uk-icon uk-slidenav-next uk-visible@m" href="#" uk-slider-item="next"><span uk-icon="icon:  chevron-right"></span></a>

            <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin">
              <li uk-slider-item="0" class=""><a href=""></a></li>
              <li uk-slider-item="1" class=""><a href=""></a></li>
              <li uk-slider-item="2" class="uk-active"><a href=""></a></li>
            </ul>
          </div>
    </div>
    <div class="uk-container">
      <div class="uk-grid">
        <div class="uk-section uk-width-3-4@l">
          <?php
          // Get the 3 most recent posts
          // the query
          $the_query = new WP_Query(array(
            'posts_per_page' => 3,
          ));
          ?>
          <?php


          while ($query2->have_posts()) : $query2->the_post(); ?>

            <!-- article -->
            <?php get_template_part('includes/post' , 'single'); ?>
            <!-- /article -->

          <?php endwhile; ?>
          <?php
          // Custom pagination for a custom query
          $big = 999999999; // need an unlikely integer

        //   echo paginate_links(array(
        //     'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        //     'format' => '?paged=%#%',
        //     'current' => max(1, get_query_var('paged')),
        //     'total' => $query2->max_num_pages,
        //   ));
        get_template_part('includes/pagination');
          ?>
        <?php else : ?>
          <p><?php esc_html_e('Sorry, no posts matched your criteria.'); ?></p>
        <?php endif; ?>
        </div>
        <div class="uk-section uk-width-1-4 uk-visible@l">
          <?php get_sidebar(); ?>
        </div>
      </div>
    </div>
  </div>

</main>

<?php get_footer(); ?>