# Base Camp Starter Theme
Base Camp is a starter theme for all Summit Group Wordpress Projects, the aim is to unify the development process across the team so you never have to ask **"How do I get this project up and running again?"**

## Getting Started
Here is a list of the available commands and what they do:

| Command                | Description |
| -----------            | ----------- |
| `npm install`          | Installs all of the required node dependencies for development     |
| `npm run new`          | Goes through the initial site setup process with CLI prompts. [^1] |
| `npm run init`         | Downloads and installs needed files for continued development.[^2] |
| `npm run dev` / `gulp` | Starts the development server and watches for file changes         |
| `npm run css`          | Compiles all css (parent and child) without needing dev server[^3] |

[^1]: Only use this command when setting up a website for the first time, the CLI prompts will overwrite necessary settings and will break the website.
[^2]: This command will skip the CLI prompts and only download/install wordpress so you can get programming faster. 
[^2]: To only compile css for one theme use `gulp css` or `gulp parentcss`

### Notes:
The `.gitignore` file is designed to exclude all of the default wordpress files so that we don't waste space in our repositories.
If you need to include a file and have questions ask Ryan Horrocks