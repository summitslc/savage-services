/*
Gulp wishlist
======================
- Version 4
- Set explicit versions for packages
- Source map for styles.min.css
- Break out tasks for parent theme and child theme
- Auto prefixer (-moz, -webkit, etc)
- Minify and concatinate
- Browser sync
  - LESS and PHP
- 

*/

/* GLOBAL DEPENDENCIES */
const { gulp, series, parallel, dest, src, watch } = require('gulp');
const del = require('del');
const fs = require('fs');
const log = require('fancy-log');

/* SETUP DEPENDENCIES */
const inject = require('gulp-inject-string');
const remoteSrc = require('gulp-remote-src');
const replace = require('gulp-replace');
const zip = require('gulp-vinyl-zip');
const inquirer = require('inquirer');

/* DEV DEPENDENCIES */
var LessAutoprefix = require('less-plugin-autoprefix');
const autoprefix = new LessAutoprefix({
    browsers: [
        "> 1%",
        "last 2 versions",
        "not ie <= 8"
    ]
});
var LessPluginCleanCSS = require('less-plugin-clean-css'),
cleanCSSPlugin = new LessPluginCleanCSS({advanced: true});
const rename = require('gulp-rename');
const browserSync = require('browser-sync');
const connect = require('gulp-connect-php');
const less = require("gulp-less");
const sourcemaps = require('gulp-sourcemaps');

const cache = require('gulp-cache');
const imagemin = require('gulp-imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminZopfli = require('imagemin-zopfli');
const imageminMozjpeg = require('imagemin-mozjpeg'); //need to run 'brew install libpng'
const imageminGiflossy = require('imagemin-giflossy');



/* ========== GULPFILE Config ========== */
const parentTheme = {
    directory: 'base-camp',
    name: 'TSG Base Camp'
}; /* URL safe: this will be the folder name */
const childTheme = {
    directory: 'savage-services',
    name: 'SavageServices2020'
}; /* URL safe: this will be the folder name */
const localDevUrl = 'base-camp.local.com';

/* ========== CSS TO INCLUDE FROM NPM ========== */
const npmStyles = [];

/* ========== JS TO INCLUDE FROM NPM ========== */
const npmScripts = [];







/* -------------------------------------------------------------------------------------------------
Installation Tasks
-------------------------------------------------------------------------------------------------- */

async function downloadWordPress() {
    await remoteSrc(['latest.zip'], {
        base: 'https://wordpress.org/',
    }).pipe(dest('./'));
}

async function unzipWordPress() {
    return await zip.src('./latest.zip').pipe(dest('./temp/'));
}

async function clearUselessFiles() {
    await del(['./temp/wordpress/wp-content/**', './latest.zip']);
}


async function removeWP() {
    const x = await del(['./*', '!./node_modules', '!./wp-content', '!./README.md', '!./wp-config.php', '!./package.json', '!./package-lock.json', '!./gulpfile.js', '!./gitignore', '!./.babelrc'], {dryRun: false});
    console.log('Deleted Files and Folders:\n', x.join('\n'))
}

exports.removewp = removeWP;

async function clearTempFolder() {
    await del(['./temp/**']);
}

async function copyConfig() {
    if (await fs.existsSync('./wp-config.php')) {
        return src('./wp-config.php')
            .pipe(inject.after("define('DB_COLLATE', '');", "\ndefine('DISABLE_WP_CRON', true);"))
            .pipe(dest('./temp/wordpress'));
    }
}

async function moveWpFiles() {
    return await src('./temp/wordpress/**').pipe(dest('./'));
}

async function freshInstallChanges() {

    return inquirer.prompt([{
            type: 'input',
            name: 'humanname',
            message: 'Enter the Human Readable Theme Name'
        },
        {
            type: 'input',
            name: 'themedirectory',
            message: 'Theme Directory name (must be URL safe)',
            validate: function(val) {
                if (!val || val.trim() == "") {
                    return 'directory is required'
                } else if (!RegExp('^[a-zA-Z0-9_-]*$').test(val)) {
                    return 'directory must be URL safe. (Letters, numbers, \'-\', \'_\' only)';
                }
                return true;
            }
        },
        {
            type: 'input',
            name: 'devName',
            message: 'Developer Name'
        },
        {
            type: 'confirm',
            name: 'updateconfig',
            message: 'Would you like to update wp-config?'
        },
        {
            type: 'input',
            name: 'dbname',
            message: 'Database Name (Leave blank to skip)',
            default: 'base_camp',
            validate: function(val) {
                if ((val && val.trim() != "") && !RegExp('^[a-zA-Z0-9_]*$').test(val)) {
                    return 'Database Name can contain Letters, numbers, \'_\' only';
                }
                return true;
            },
            when: answers => {
                return answers.updateconfig;
            }
        },
        {
            type: 'input',
            name: 'dbuser',
            default: 'root',
            message: 'Database User (Leave blank to skip)',
            validate: function(val) {
                if ((val && val.trim() != "") && !RegExp('^[a-z_]*$').test(val)) {
                    return 'Database User can contain lowercase letters and \'_\' only';
                }
                return true;
            },
            when: answers => {
                return answers.updateconfig;
            }
        },
        {
            type: 'input',
            name: 'dbpass',
            default: 'tsgMysql',
            message: 'Database Password (Leave blank to skip)',
            when: answers => {
                return answers.updateconfig;
            }
        },
        {
            type: 'input',
            name: 'dbhost',
            message: 'Database Host (Leave blank to skip)',
            default: '10.0.0.29',
            when: answers => {
                return answers.updateconfig;
            }
        },


    ]).then(
        answers => {
            console.log(JSON.stringify(answers, null, '  '));
/* add function to replace table prefix: regex -> (table_prefix[\s=]*)(.*)(;) */
/* add function to update salts/auth keys: regex -> (\/\*\*#@\+[\s\S]*\*\/)([\s\S\n]*)(\/\*\*#@-\*\/) */
            return series(
                async function() {
                    log(childTheme.directory);
                    return fs.rename('./wp-content/themes/' + childTheme.directory, './wp-content/themes/' + answers.themedirectory, function(err) {
                        if (err) {
                            log('error-fdsjkhfjdsk: ' + err);
                            throw err;
                        }
                    });
                },
                async function() {
                    return src('./gulpfile.js', { base: './' })
                        .pipe(replace(/(childTheme[\n\t\r\s{=:a-zA-Z]+')(.+)(',[\n\t\r\s{=:a-zA-Z']+')(.+)(')/, "$1" + answers.themedirectory + "$3" + answers.humanname + "$5"))
                        .pipe(dest('./'));

                },
                async function() {
                    return src('./wp-content/themes/' + answers.themedirectory + '/style.css', { base: './' })
                        .pipe(replace(/(Theme Name: )(.+)/, "$1" + answers.humanname))
                        .pipe(replace(/(Author:[\sa-zA-Z]+\()(.+)(\))/, "$1" + answers.devName + "$3"))
                        .pipe(dest('./'));
                },
                async function() {
                    return src('./wp-content/themes/base-camp-setup/functions.php', { base: './' })
                        .pipe(replace('base-camp-child', answers.themedirectory))
                        .pipe(dest('./'));
                },
                async function() {
                    if (!answers.updateconfig)
                        return;
                    return src('./wp-config.php', { base: './' })
                        .pipe(replace(/('DB_NAME', ')(.+)(')/g, "$1" + answers.dbname + "$3"))
                        .pipe(replace(/('DB_USER', ')(.+)(')/g, "$1" + answers.dbuser + "$3"))
                        .pipe(replace(/('DB_PASSWORD', ')(.+)(')/g, "$1" + answers.dbpass + "$3"))
                        .pipe(replace(/('DB_HOST', ')(.+)(')/g, "$1" + answers.dbhost + "$3"))
                        .pipe(dest('./'));
                },
            )();

        }
    );


}

exports.setup = downloadWordPress;
exports.unzipwp = unzipWordPress;
exports.install = series(clearUselessFiles, moveWpFiles);
exports.clearTemp = clearTempFolder;

exports.fresh = freshInstallChanges;


/* -------------------------------------------------------------------------------------------------
Development Tasks
-------------------------------------------------------------------------------------------------- */
exports.default = series(css, devServer);
exports.css = css;
exports.parentcss = parentcss;
exports.tinypng = imgOptimize;

function devServer() {
    connect.server({
        base: './',
        port: '3020',
        stdio: 'ignore',
    },
        () => { 
            browserSync({
                logPrefix: 'Base Camp Child',
                // proxy: 'savage2020.test',
                proxy: '127.0.0.1:3020',
                host: '127.0.0.1',
                port: '3010',
                open: 'external',
            });
        }
    );

    watch('./wp-content/themes/' + parentTheme.directory + '/styles/**/*.less', parentcss);
    watch('./wp-content/themes/' + parentTheme.directory + '/**/*.php', Reload);
    watch('./wp-content/themes/' + parentTheme.directory + '/**/*.html', Reload);
    watch('./wp-content/themes/' + parentTheme.directory + '/**/*.js', Reload); /* add some more JS handling here */

    
    watch('./wp-content/themes/' + childTheme.directory + '/styles/**/*.less', css);
    watch('./wp-content/themes/' + childTheme.directory + '/**/*.php', Reload);
    watch('./wp-content/themes/' + childTheme.directory + '/**/*.html', Reload);
    
    watch('./wp-content/themes/' + childTheme.directory + '/images/unoptimized/**/*', imgOptimize); /* watch images and auto-optimize them */

    watch('./wp-content/themes/' + childTheme.directory + '/**/*.js', Reload); /* add some more JS handling here */
}

function Reload(done) {
    browserSync.reload();
    done();
}
async function css() {
    return src('./wp-content/themes/' + childTheme.directory + '/styles/main.less')
        .pipe(sourcemaps.init())
        .pipe(less({
            plugins: [autoprefix, cleanCSSPlugin]
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(rename("style.min.css"))
        .pipe(dest('./wp-content/themes/' + childTheme.directory))
        .pipe(browserSync.stream({ match: '**/*.css' }));
    

}
async function parentcss() {
    return src('./wp-content/themes/' + parentTheme.directory + '/styles/main.less')
        .pipe(sourcemaps.init())
        .pipe(less({
            plugins: [autoprefix, cleanCSSPlugin]
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(rename("style.min.css"))
        .pipe(dest('./wp-content/themes/' + parentTheme.directory))
        .pipe(browserSync.stream({ match: '**/*.css' }));
    

}

function imgOptimize() {


    return src(['./wp-content/themes/' + childTheme.directory + '/images/unoptimized/**/*.{gif,png,jpg}'])
    .pipe(cache(imagemin([
        //png
        imageminPngquant({
            speed: 1,
            quality: [0.95, 1] //lossy settings
        }),
        imageminZopfli({
            more: true
            // iterations: 50 // very slow but more effective
        }),
        //gif
        // imagemin.gifsicle({
        //     interlaced: true,
        //     optimizationLevel: 3
        // }),
        //gif very light lossy, use only one of gifsicle or Giflossy
        imageminGiflossy({
            optimizationLevel: 3,
            optimize: 3, //keep-empty: Preserve empty transparent frames
            lossy: 2
        }),
        //svg
        imagemin.svgo({
            plugins: [{
                removeViewBox: false
            }]
        }),
        //jpg lossless
        imagemin.jpegtran({
            progressive: true
        }),
        //jpg very light lossy, use vs jpegtran
        imageminMozjpeg({
            quality: 90
        })
    ])))
    .pipe(dest('./wp-content/themes/' + childTheme.directory + '/images'));

}

/* -------------------------------------------------------------------------------------------------
Production Tasks
-------------------------------------------------------------------------------------------------- */





/* -------------------------------------------------------------------------------------------------
Messages
-------------------------------------------------------------------------------------------------- */
const date = new Date().toLocaleDateString('en-GB').replace(/\//g, '.');
const devServerReady = 'Base Camp is ready, start the workflow with the command: $ \x1b[1mnpm run dev\x1b[0m';